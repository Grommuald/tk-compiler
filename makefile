CPP=g++
CC=gcc

CPP_FLAGS=-std=c++14 -g

INCLUDE_DIR=./include/
SOURCE_DIR=./src/
BIN_DIR=./bin/
OUTPUT_DIR=./output/

OUTPUT_DIR_CREATE=mkdir -p $(OUTPUT_DIR)

LEX=flex
LEX_TARGET=lex.yy.o
YACC=bison
YACC_TARGET=def.tab.cc
YACC_OPT=-ll -DYYDEBUG

TARGET=compiler

OBJ=$(SOURCE_DIR)*.o
OBJ_DST=$(OUTPUT_DIR)*.o

all:\
	$(TARGET)

$(TARGET): $(YACC_TARGET) $(LEX_TARGET) $(OBJ)
	@echo "COMPILER:"
	$(CPP) $(CPP_FLAGS) $(OBJ_DST) -o $(TARGET) $(YACC_OPT)
	rm -rf $(INCLUDE_DIR)def.tab.hh $(SOURCE_DIR)def.tab.cc $(SOURCE_DIR)lex.yy.c
	rm -rf $(BIN_DIR)$(TARGET) $(BIN_DIR)*.asm
	mkdir -p $(BIN_DIR)
	mv $(TARGET) $(BIN_DIR)

$(LEX_TARGET): 
	@echo "LEX:"
	$(LEX) $(SOURCE_DIR)lexer.l
	mv lex.yy.c $(SOURCE_DIR)
	$(CC) -I $(INCLUDE_DIR) -c $(SOURCE_DIR)lex.yy.c
	mkdir -p output
	mv *.o $(OUTPUT_DIR)

$(YACC_TARGET):	
	@echo "YACC:"
	$(YACC) -d $(SOURCE_DIR)def.yy
	mv def.tab.cc $(SOURCE_DIR)
	mv def.tab.hh $(INCLUDE_DIR)
	$(CPP) $(CPP_FLAGS) -I $(INCLUDE_DIR) -c $(SOURCE_DIR)$@
	
%.o: %.cpp
	@echo "UNIVERSAL:"
	$(CPP) $(CPP_FLAGS) -I $(INCLUDE_DIR) -c $^
	mv *.o $(OUTPUT_DIR)