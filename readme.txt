Features:

1) List of keywords:
    else, false, for, if, int, new, real, true, while

2) C-like syntax

3) Each identifier requires its strong-typed declaration:
    int foo;
    real bar = 3.14;

4) Simple dynamic memory allocation:
    int* foo = new int[2+5];
    int* bar = foo;

    foo[4] = 2;
    bar[0] = 4; // == foo[0]

5) No actual variables scopes 

6) Input: inputi(), inputr()

7) Output: print(), println(), printi(), printiln(), printr(), printrln()

8) print() and println() support embedded format specifiers:
    println( "Test: %d and %f", 12, 3.14 );     // prints: "Test: 12 and 3.14"

How to build the compiler and run your first program?
1) Download: https://courses.missouristate.edu/KenVollmar/mars/MARS_4_5_Aug2014/Mars4_5.jar

2) Go to the Terminal:
    > cd tk-compiler/
    > make
    > bin/compiler your_code.cmm 
    > java -jar Mars4_5.jar bin/generatedCode.asm 

3) You have just run your first program!


