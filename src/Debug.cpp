#include "Debug.hpp"
#include <iostream>
#include <cstdarg>

using namespace std;

bool DebugLogger::enabled = false;

void DebugLogger::log( const std::string& callerName, const std::string& message... )
{
    if( enabled == false )
        return;

    va_list args;
    va_start( args, message );

    cout << callerName << ": ";
    
    bool lastSpecial = false;

    for( uint32_t i = 0; i < message.length() - 1; ++i )
    {
        char c = message[i];

        if( c == '%' )
        {
            lastSpecial = true;

            char next = message[i+1];
            if( next == 'd' )
            {
                int arg = va_arg( args, int );
                cout << arg;
            }
            else if( next == 'f' ) 
            {
                double arg = va_arg( args, double );
                cout << arg;
            }
            else if( next == 's' )
            {
                string arg = va_arg( args, const char* );
                cout << arg;
            }
            else 
            {
                cout << next;
            }
            ++i;
        }
        else 
        {
            lastSpecial = false;
            cout << c;
        }
    }
    va_end( args );
    if( lastSpecial == false )
    {
        cout << *(--message.end());
    }
    cout << endl;
}

void DebugLogger::setEnabled()
{
    enabled = true;
}