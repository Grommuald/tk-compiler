#include "CodeGenerator.hpp"
#include "LabelStack.hpp"
#include "Symbolic.hpp"
#include "PrintWrapper.hpp"
#include "Debug.hpp"

#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <iostream>
#include <iterator>
#include <algorithm>

using namespace std;

extern LabelStack& labelStack;
extern PrintWrapper& printWrapper;

extern "C" int yyerror( const char* msg, ... );

CodeGenerator& CodeGenerator::Instance()
{
    static CodeGenerator myInstance;
    return myInstance;
}

CodeGenerator::CodeGenerator()
{
    m_outputFile.open( "bin/generatedCode.asm" );
    putData( ".data" );
    putText( ".text" );
}

CodeGenerator::~CodeGenerator()
{
    sort( m_dataSection.begin(), m_dataSection.end() );
    for( const auto& i : m_dataSection )
        m_outputFile << i << endl;

    m_outputFile << endl;

    trimRedundantSaveLoads();

    for( const auto& i : m_textSection )
        m_outputFile << i << endl;

    m_outputFile.close();
}

void CodeGenerator::trimRedundantSaveLoads()
{
    if( m_textSection.size() >= 3 )
    {
        for( auto i = m_textSection.begin()+1; i != --m_textSection.end(); )
        {
            stringstream currentLineTokens( *i );
            stringstream nextLineTokens( *(i+1) );

            istream_iterator<string> itCurr( currentLineTokens );
            decltype( itCurr ) endCurr;

            istream_iterator<string> itNext( nextLineTokens );
            decltype( itNext ) endNext;

            vector<string> currentLineTokensVector( itCurr, endCurr );
            vector<string> nextLineTokensVector( itNext, endNext );

            if( 
                ( 
                ( currentLineTokensVector[0] == "sw" && nextLineTokensVector[0] == "lw" )
                || ( currentLineTokensVector[0] == "s.s" && nextLineTokensVector[0] == "l.s" )
                || ( currentLineTokensVector[0] == "sw" && nextLineTokensVector[0] == "l.s" ) 
                )
                && currentLineTokensVector[1] == nextLineTokensVector[1] 
                && currentLineTokensVector[2] == nextLineTokensVector[2] )
            {
                m_textSection.erase( i+1 );
                ++i;
            }
            else if( 
                currentLineTokensVector[0] == "lw" && nextLineTokensVector[0] == "sw" 
                && currentLineTokensVector[1] != nextLineTokensVector[1] 
                && currentLineTokensVector[2] == nextLineTokensVector[2] )
            {
                i = m_textSection.erase( i );
            }
            else
            {
                ++i;
            }
        }
    }
}

void CodeGenerator::putText( const std::string& line )
{
    m_textSection.push_back( line );
}

void CodeGenerator::putData( const std::string& line )
{
    m_dataSection.push_back( line );
}

void CodeGenerator::pushData(
    const idSymbolPair& data,
    const int& startingValue
)
{
    putData( data.first + ": .word " + to_string( startingValue ) );
}

void CodeGenerator::pushData(
    const idSymbolPair& data,
    const float& startingValue
)
{
    putData( data.first + ": .float  " + to_string( startingValue ) );
}

void CodeGenerator::pushData(
    const idSymbolPair& data,
    string startingValue
)
{
    startingValue.erase(
        std::remove_if( startingValue.begin(), startingValue.end(),
        [] (unsigned char x) {  return x == '"'; }
    ),  startingValue.end() );

    putData( data.first + ": .asciiz  \"" + startingValue + "\"" );
}

void CodeGenerator::pushData( const idSymbolPair& data )
{
    string line = data.first + ": ";

    if( SymbolGetter::getSpecial( data ) == SymbolType::Variable )
    {
        switch( SymbolGetter::getType( data ) )
        {
            case TypeSpecifier::Integer:
                line += ".word 0";
                break;

            case TypeSpecifier::Real:
                line += ".float 0.0";
                break;
                
        }
    }
    else if( SymbolGetter::getSpecial( data ) == SymbolType::Array )
    {
       line += ".space " + to_string( SymbolGetter::getArraySuffixIfPossible( data )->m_numberOfBytesToAllocate );
    }
    else if( SymbolGetter::getSpecial( data ) == SymbolType::Address )
    {
        line += ".word 0";
    }
    putData( line );
}

void CodeGenerator::loadOperandsToRegisters(
    std::initializer_list< idSymbolPair > operands,
    const std::string& registerName,
    const bool& useNumeration,
    const bool& forceInteger
)
{
    uint8_t registerNumber = 0;

    for( auto& i : operands )
    {
        //placeMarker( "operand: " + SymbolGetter::getId( i ) );
        string snippet = "";

        if( forceInteger )
        {
            switch( SymbolGetter::getSpecial( i ) )
            {
                case SymbolType::Literal:
                    snippet += "li ";
                    break;

                case SymbolType::Variable:
                    snippet += "lw ";
                    break;
            }
        }
        else if( SymbolGetter::isString( i ) )
        {
            snippet += "la ";
        }
        else if( SymbolGetter::isArray( i ) )
        {
            snippet += "la ";
        }
        else if( SymbolGetter::isInteger( i ) || SymbolGetter::isAddress( i ) )
        {
            switch( SymbolGetter::getSpecial( i ) )
            {
                case SymbolType::Literal:
                    snippet += "li ";
                    break;
                    
                case SymbolType::Address:
                case SymbolType::Variable:
                    snippet += "lw ";
                    break;
            }
        }
        else if( SymbolGetter::isReal( i ) )
        {
            snippet += "l.s ";
        }

        snippet += "$" + registerName;

        if ( useNumeration )
            snippet += to_string( registerNumber++ );

        snippet += ", " + SymbolGetter::getId( i );
        putText( snippet );
    }
}

void CodeGenerator::setValue( const idSymbolPair& lhs, const idSymbolPair& rhs, const string& resultVarName )
{
    // rhs = lhs ( please, don't ask me why )
    //placeMarker( "setValue: begin" );
    if( SymbolGetter::isReal( lhs ) && SymbolGetter::isReal( rhs ) )
    {
        loadOperandsToRegisters( { lhs, rhs }, "f" );
        putText( "s.s $f0, " + SymbolGetter::getId( rhs ) );
    }
    else if( SymbolGetter::isReal( lhs ) && SymbolGetter::isInteger( rhs ) )
    {
        castToInt( lhs, rhs );
    }
    else if( SymbolGetter::isInteger( lhs ) && SymbolGetter::isReal( rhs ) )
    {
        castToFloat( lhs, rhs );
    }
    else if( SymbolGetter::isInteger( lhs ) && SymbolGetter::isInteger( rhs ) )
    {
        loadOperandsToRegisters( { lhs, rhs } );
        putText( "sw $t0, " + SymbolGetter::getId( rhs ) ) ;
    }
    else if( SymbolGetter::getAddressSuffixIfPossible( lhs ) )
    {
        if( SymbolGetter::isAddress( rhs ) )
        {
            if( SymbolGetter::getAddressSuffixIfPossible( lhs )->m_pointedType != 
                SymbolGetter::getAddressSuffixIfPossible( rhs )->m_pointedType )
            {
                yyerror( "Cannot assign a pointer to a pointer of another type." );
            }

            putText( "lw $t0, " + SymbolGetter::getId( lhs ) );
            putText( "sw $t0, " + SymbolGetter::getId( rhs ) );
        }   
        else
        {
            yyerror( "Cannot assign an address to a non-pointer variable." );
        }    
    }
    else if( SymbolGetter::isAddress( rhs ) )
    {
        switch( SymbolGetter::getAddressSuffixIfPossible( rhs )->m_pointedType )
        {
            case TypeSpecifier::Integer:
                loadOperandsToRegisters( { lhs }, "t" );
                break;

            case TypeSpecifier::Real:
                loadOperandsToRegisters( { lhs }, "f" );
                putText( "mfc1 $t0, $f0" );
                break;
        }
        putText( "lw $t1, " + SymbolGetter::getId( rhs ) );
        putText( "sw $t0, ($t1)" ) ;
    }
    //placeMarker( "setValue: end" );
}

void CodeGenerator::pushBinaryOperation(
    const std::string& op,
    const idSymbolPair& lhs,
    const idSymbolPair& rhs,
    const std::string& resultVarName
)
{
    string line = "";

    vector<idSymbolPair> operands = { lhs, rhs };

    auto stringOperand =
        find_if( operands.begin(), operands.end(),
        [] ( const idSymbolPair& operand )
        {
            return SymbolGetter::isString( operand );
        } );

    if( stringOperand != operands.end() )
    {
        string message = "Invalid operand of type \"string\" to binary operator: " + op;
        yyerror( message.c_str() );
    }

    if( op == "=" )
    {
        setValue( lhs, rhs, resultVarName );
    }
    else
    {
        vector< idSymbolPair > operands = { lhs, rhs };

        bool isFloatNumbersOperation =
            any_of( operands.begin(), operands.end(),
            [ ] ( const idSymbolPair& operand )
            {
                return SymbolGetter::isReal( operand );
            });

        if( op == "+" || op == "-" || op == "*" || op == "/" || op == "%" )
        {
            if( isFloatNumbersOperation )
                loadOperandsToRegisters( { rhs, lhs }, "f" );
            else
                loadOperandsToRegisters( { rhs, lhs } );

            if( op == "*" )
                line += "mul";
            else if( op == "+" )
                line += "add";
            else if( op == "/" || op == "%" )
                line += "div";
            else if( op == "-" )
                line += "sub";

            if( isFloatNumbersOperation )
            {
                line += ".s";
            }

            if ( op == "%" )
            {
                if( isFloatNumbersOperation )
                {
                    yyerror( "Invalid operands of type: real to % operation." );
                }
                line += " $t0, $t1";
                putText( line );
                line = "mfhi $t0";
            }
            else
            {
                if( isFloatNumbersOperation )
                    line += " $f0, $f0, $f1";
                else
                    line += " $t0, $t0, $t1";
            }
            putText( line );

            if( isFloatNumbersOperation )
                putText( "s.s $f0, " + resultVarName );
            else
                putText( "sw $t0, " + resultVarName );
        }
        else if(
            op == "==" || op == "!=" ||
            op == ">=" || op == "<=" ||
            op == ">" || op == "<")
        {
            if( isFloatNumbersOperation )
                loadOperandsToRegisters( { rhs, lhs }, "f" );
            else
                loadOperandsToRegisters( { rhs, lhs } );

            if( op == "==" || op == "!=" )
            {
                if( isFloatNumbersOperation )
                {
                    if( op == "==" )
                        setRegisterIfFloatComparisonTrue( "c.eq.s $f0, $f1", resultVarName );
                    else
                        setRegisterIfFloatComparisonTrue( "c.ne.s $f0, $f1", resultVarName );
                }
                else
                {
                    if( op == "==" )
                        setRegisterIfIntComparisonTrue( "beq $t0, $t1", resultVarName );
                    else
                        setRegisterIfIntComparisonTrue( "bne $t0, $t1", resultVarName );
                }
            }
            else if( op == "<" )
            {
                if( isFloatNumbersOperation )
                    setRegisterIfFloatComparisonTrue( "c.lt.s $f0, $f1", resultVarName );
                else
                    setRegisterIfIntComparisonTrue( "blt $t0, $t1", resultVarName );
            }
            else if( op == ">" )
            {
                if( isFloatNumbersOperation )
                    setRegisterIfFloatComparisonTrue( "c.le.s $f1, $f0", resultVarName );
                else
                    setRegisterIfIntComparisonTrue( "bgt $t0, $t1", resultVarName );
            }
            else if( op == ">=" )
            {
                if( isFloatNumbersOperation )
                    setRegisterIfFloatComparisonTrue( "c.lt.s $f1, $f0", resultVarName );
                else
                    setRegisterIfIntComparisonTrue( "bge $t0, $t1", resultVarName );
            }
            else if( op == "<=" )
            {
                if( isFloatNumbersOperation )
                    setRegisterIfFloatComparisonTrue( "c.le.s $f0, $f1", resultVarName );
                else
                    setRegisterIfIntComparisonTrue( "ble $t0, $t1", resultVarName );
            }

        }
        else if( op == "&&" || op == "||" )
        {
            loadOperandsToRegisters( { lhs, rhs }, "t", true, true );

            putText( "li $t2, 0" );

            if( op == "&&" )
            {
                putText( "sltu $t0, $t2, $t0" );
                putText( "sltu $t1, $t2, $t1" );
                putText( "and $t0, $t0, $t1" );
                putText( "li $t2, 1" );
                setRegisterIfIntComparisonTrue( "beq $t0, $t2", resultVarName );
            }
            else if( op == "||" )
            {
                putText( "sltu $t0, $t2, $t0" );
                putText( "sltu $t1, $t2, $t1" );
                putText( "or $t0, $t0, $t1" );
                putText( "li $t2, 1" );
                setRegisterIfIntComparisonTrue( "beq $t0, $t2", resultVarName );
            }
        }
        else if( op == "[]" )
        {
            loadAddressToVariable( 
                lhs, 
                rhs, 
                resultVarName 
            );
        }
    }
}

void CodeGenerator::pushUnaryOperation(
    const std::string& op,
    const idSymbolPair& rhs,
    const std::string& resultVarName
)
{
    auto checkForString = [&]
    {
        if( SymbolGetter::isString( rhs ) )
        {
            string message = "Invalid operand of type \"string\" to unary operator: " + op;
            yyerror( message.c_str() );
        }
    };

    if( op == "!" || op == "-" )
    {
        checkForString();
    
        if( op == "!" )
        {
            string setToOneBranch = getCurrentBranchFlagName();
            ++m_branchFlagNumber;

            if( SymbolGetter::isReal( rhs ) )
            {
                loadOperandsToRegisters( { rhs }, "f" );

                putText( "cvt.w.s $f0, $f0" );
                putText( "mfc1 $t0, $f0" );
            }
            else
            {
                loadOperandsToRegisters( { rhs } );
            }

            putText( "beqz $t0, " + setToOneBranch );
            putText( "li $t0, 0" );
            putText( "j " + getCurrentBranchFlagName() );
            putText( setToOneBranch + ": " );
            putText( "li $t0, 1" );
            putText( getCurrentBranchFlagName() + ": " );

            putText( "sw $t0, " + resultVarName );
            ++m_branchFlagNumber;
        }
        else if( op == "-" )
        {
            if( SymbolGetter::isReal( rhs ) )
            {
                loadOperandsToRegisters( { rhs }, "f" );
                putText( "neg.s $f0, $f0" );
                putText( "s.s $f0, " + resultVarName );
            }
            else
            {
                loadOperandsToRegisters( { rhs } );
                putText( "neg $t0, $t0" );
                putText( "sw $t0, " + resultVarName );
            }
        }
    }
    else if( op == "post_inc" || op == "post_dec" )
    {
        checkForString();
        loadOperandsToRegisters( { rhs } );

        if( resultVarName != "" )
            putText( "sw $t0 " + resultVarName );

        if( op == "post_inc" )
            putText( "addi $t0, $t0, 1" );
        else if( op == "post_dec" )
            putText( "subi $t0, $t0, 1" );

        putText( "sw $t0, " + SymbolGetter::getId( rhs ) );
    }
    else if( op == "pre_inc" || op == "pre_dec" )
    {
        checkForString();
        loadOperandsToRegisters( { rhs } );

        if( op == "pre_inc" )
            putText( "addi $t0, $t0, 1" );
        else if( op == "pre_dec" )
            putText( "subi $t0, $t0, 1" );

        if( resultVarName != "" )
            putText( "sw $t0 " + resultVarName );

        putText( "sw $t0, " + SymbolGetter::getId( rhs ) );
    }
    else if( op == "if" || op == "while" || op == "for" )
    {
        checkForString();
        loadOperandsToRegisters( { rhs }, "t", true, true );

        putText( "beqz $t0, " + labelStack.getTop() );
    }
    else if( op == "print" || op == "printi" || op == "printr"
            || op == "println" || op == "printiln" || op == "printrln" )
    {
        string line = "li $v0, ";

        if( SymbolGetter::isInteger( rhs ) )
        {
            line += "1";
            putText( line );
            loadOperandsToRegisters( { rhs }, "a" );
        }
        else if( SymbolGetter::isReal( rhs ) )
        {
            line += "2";
            putText( line );
            loadOperandsToRegisters( { rhs }, "f12", false );
        }
        else if( SymbolGetter::isString( rhs ) )
        {
            line += "4";
            putText( line );
            loadOperandsToRegisters( { rhs }, "a" );
        }
        putText( "syscall" );

        if( op == "println" || op == "printiln" || op == "printrln" )
            writeNewLine();
    }
    else if( op == "print_token" )
    {
        printWrapper.passToken( rhs );
    }
    else if( op == "new" )
    {
        putText( "li $v0, 9 " );
        loadOperandsToRegisters( { rhs }, "a" );
        putText( "syscall" );
        putText( "sw $v0, " + resultVarName );
    }
}

void CodeGenerator::placeLabel( const std::string& labelName )
{
    putText( labelName + ":" );
}

void CodeGenerator::placeJumpTo( const std::string& labelName )
{
    putText( "j " + labelName );
}

void CodeGenerator::loadInputTo( const idSymbolPair& symbol )
{
    if( SymbolGetter::isInteger( symbol ) )
    {
        putText( "li $v0 5" );
        putText( "syscall" );
        putText( "sw $v0, " + SymbolGetter::getId( symbol ) );
    }
    else if ( SymbolGetter::isReal( symbol ) )
    {
        putText( "li $v0 6" );
        putText( "syscall" );
        putText( "s.s $f0, " + SymbolGetter::getId( symbol ) );
    }
}

void CodeGenerator::castToFloat(
    const idSymbolPair& src,
    const idSymbolPair& dst
)
{
    loadOperandsToRegisters( { src } );
    putText( "mtc1 $t0, $f0" );
    putText( "cvt.s.w $f0, $f0" );
    putText( "s.s $f0, " + SymbolGetter::getId( dst ) );
}

void CodeGenerator::castToInt(
    const idSymbolPair& src,
    const idSymbolPair& dst
)
{
    loadOperandsToRegisters( { src }, "f" );
    putText( "cvt.w.s $f0, $f0" );
    putText( "mfc1 $t0, $f0" );
    putText( "sw $t0, " + SymbolGetter::getId( dst ) );
}

void CodeGenerator::dereferenceToInt(
    const idSymbolPair& src,
    const idSymbolPair& dst
)
{
    loadOperandsToRegisters( { src } );
    putText( "lw $t1, ($t0)" );
    putText( "sw $t1, " + SymbolGetter::getId( dst ) );
}

void CodeGenerator::dereferenceToFloat(
    const idSymbolPair& src,
    const idSymbolPair& dst
)
{
    loadOperandsToRegisters( { src } );
    putText( "lw $t1, ($t0)" );
    putText( "mtc1 $t1, $f0" );
    putText( "s.s $f0, " + SymbolGetter::getId( dst ) );
}

std::string CodeGenerator::getCurrentBranchFlagName() const
{
    return "BF" + to_string( m_branchFlagNumber );
}

void CodeGenerator::setRegisterIfFloatComparisonTrue(
    const std::string& condition,
    const std::string& resultVarName
)
{
    putText( "li $t0, 0" );
    putText( condition );

    putText( "bc1f " + getCurrentBranchFlagName() );
    putText( "li $t0, 1" );
    putText( getCurrentBranchFlagName() + ":" );

    putText( "sw $t0, " + resultVarName );

    ++m_branchFlagNumber;
}

void CodeGenerator::setRegisterIfIntComparisonTrue(
    const std::string& condition,
    const std::string& resultVarName
)
{
    putText( "li $t2, 1" );
    putText( condition + ", " + getCurrentBranchFlagName() );
    putText( "li $t2, 0" );
    putText( getCurrentBranchFlagName() + ":" );

    putText( "sw $t2, " + resultVarName );

    ++m_branchFlagNumber;
}

void CodeGenerator::zeroAddress( 
    const idSymbolPair& addressVariable 
)
{
     putText( "li $t0, 0" );
     putText( "sw $t0, " + SymbolGetter::getId( addressVariable ) );
}

void CodeGenerator::loadAddressToVariable(
    const idSymbolPair& indexValueVariable,
    const idSymbolPair& addressVariable,
    const std::string& resultVarName
)
{   
    DebugLogger::log( __FUNCTION__, "entering" );
    if( AddressSuffix* addrSuff = SymbolGetter::getAddressSuffixIfPossible( addressVariable ) )
    {
        if( addrSuff->m_rootArray == nullptr || addrSuff->m_currentNestLevel == addrSuff->m_rootArray->m_dimensions.size() - 1 )
        {
            loadOperandsToRegisters( { indexValueVariable, addressVariable } );

            DebugLogger::log( __FUNCTION__, "addressVariable: %s", SymbolGetter::getId( addressVariable ).c_str() );

            putText( "add $t1, $t1, $t0" );
            putText( "sll $t1, $t1, 2" );

            if( addrSuff->m_rootArray == nullptr )
                putText( "lw $t0, " + addrSuff->m_pointedArrayId );
            else 
                putText( "la $t0, " + addrSuff->m_pointedArrayId );

            putText( "add $t1, $t1, $t0" );

            addrSuff->m_isReadyToDereference = true;
            putText( "sw $t1, " + resultVarName );
        }
        else 
        {
            DebugLogger::log( __FUNCTION__, "addrSuff->m_currentNestLevel != addrSuff->m_rootArray->m_dimensions.size() - 1" );
            if( addrSuff->m_rootArray->m_dimensions.size() == 2 )
            {
                // [][]
                // ^
                loadOperandsToRegisters( { indexValueVariable } ); 
                putText( "li $t1, " + to_string( addrSuff->m_rootArray->m_dimensions[0] ) );
                putText( "mul $t1, $t1, $t0" );
                loadOperandsToRegisters( { addressVariable } );
                putText( "add $t1, $t1, $t0" );
                putText( "sw $t1, " + resultVarName );
            }
            else if( addrSuff->m_rootArray->m_dimensions.size() == 3 )
            {
                // [][][]
                // ^
                if( addrSuff->m_currentNestLevel == 0 )
                {
                    loadOperandsToRegisters( { indexValueVariable } ); 
                    putText( "li $t1, " + to_string( addrSuff->m_rootArray->m_dimensions[0] ) );
                    putText( "mul $t1, $t1, $t0" );
                    putText( "li $t0, " + to_string( addrSuff->m_rootArray->m_dimensions[1] ) );
                    putText( "mul $t1, $t1, $t0" );
                }
                // [][][]
                //   ^
                else if( addrSuff->m_currentNestLevel == 1 )
                {
                    loadOperandsToRegisters( { indexValueVariable } ); 
                    putText( "li $t1, " + to_string( addrSuff->m_rootArray->m_dimensions[0] ) );
                    putText( "mul $t1, $t1, $t0" );
                }
                loadOperandsToRegisters( { addressVariable } );
                putText( "add $t1, $t1, $t0" );
                putText( "sw $t1, " + resultVarName );
            }
            addrSuff->m_currentNestLevel++;
        }
    }
    
    DebugLogger::log( __FUNCTION__, "exiting" );
}

void CodeGenerator::writeNewLine()
{
    putText( "addi $a0, $0, 0xA" );
    putText( "addi $v0, $0, 0xB" );
    putText( "syscall" );
}

void CodeGenerator::placeMarker( const std::string& marker )
{
    putText( "#" + marker );
}

CodeGenerator& codeGenerator = CodeGenerator::Instance();
