D       [0-9]
L       [a-zA-Z_]

%{
#include <stdlib.h>
#include <string.h>
#include "def.tab.hh"

#define INFILE_ERROR    1
#define OUTFILE_ERROR   2

extern  int     yylineno;
        int     yyerror( const char*, ... );
        void    comment();
        void    count();
%}

%option yylineno

%%

"__tempRes"[0-9]+                           { yyerror("Unauthorized usage of compiler's variable."); }
"__tempAddr"[0-9]+                          { yyerror("Unauthorized usage of compiler's variable."); }
"__f"[0-9]+                                 { yyerror("Unauthorized usage of compiler's variable."); }
"__str"[0-9]+                               { yyerror("Unauthorized usage of compiler's variable."); }
"/*"                                        { comment(); }
"//"[^\n]*                                  { }

"break"                                     { count(); return BREAK; }
"contiune"                                  { count(); return CONTINUE; }
"else"                                      { count(); return ELSE; }
"false"                                     { count(); yylval.sval = strdup( yytext ); return FALSE; }
"for"                                       { count(); return FOR; }
"if"                                        { count(); return IF; }
"int"                                       { count(); yylval.sval = strdup( yytext ); return INT; }
"inputi"                                    { count(); return INPUTI; }
"inputr"                                    { count(); return INPUTR; }
"new"                                       { count(); return NEW; }
"print"                                     { count(); return PRINT; }
"printi"                                    { count(); return PRINTI; }
"printr"                                    { count(); return PRINTR; }
"println"                                   { count(); return PRINTLN; }
"printiln"                                  { count(); return PRINTILN; }
"printrln"                                  { count(); return PRINTRLN; }
"real"                                      { count(); yylval.sval = strdup( yytext ); return REAL; }
"return"                                    { count(); return RETURN; }
"true"                                      { count(); yylval.sval = strdup( yytext ); return TRUE; }
"while"                                     { count(); return WHILE; }

{L}({L}|{D})*                               { count(); yylval.sval = strdup( yytext ); return IDENTIFIER; }
0|[1-9]{D}*                                 { count(); yylval.ival = atoi( yytext ); return INT_LITERAL; }
{D}*"."{D}+                                 { count(); yylval.fval = atof( yytext ); return REAL_LITERAL; }

\"(\\.|[^\\"\n])*\"                         { count(); yylval.sval = strdup( yytext ); return STRING_LITERAL; }

"++"                                        { count(); return INC_OP; }
"--"                                        { count(); return DEC_OP; }
"&&"                                        { count(); return AND_OP; }
"||"                                        { count(); return OR_OP; }
"<="                                        { count(); return LE_OP; }
">="                                        { count(); return GE_OP; }
"=="                                        { count(); return EQ_OP; }
"!="                                        { count(); return NE_OP; }
"="                                         { count(); return '='; }
";"                                         { count(); return ';'; }
"{"                                         { count(); return '{'; }
"}"                                         { count(); return '}'; }
"("                                         { count(); return '('; }
")"                                         { count(); return ')'; }
"["                                         { count(); return '['; }
"]"                                         { count(); return ']'; }
"!"                                         { count(); return '!'; }
"-"                                         { count(); return '-'; }
"+"                                         { count(); return '+'; }
"*"                                         { count(); return '*'; }
"/"                                         { count(); return '/'; }
"%"                                         { count(); return '%'; }
"<"                                         { count(); return '<'; }
">"                                         { count(); return '>'; }
","                                         { count(); return ','; }
"."                                         { count(); return '.'; }

[ \t\v\f]                                   { count(); }
\n                                          { count(); }
.			                                { yyerror("Lexical error"); }

%%

void comment()
{
    char c, prev = 0;

    while( ( c = input() ) != 0 )
    {
        if( c == '/' && prev == '*' )
            return;
        prev = c;
    }
    yyerror( "Unterminated comment." );
}

int column = 0;

void count()
{
    int i;

    for( i = 0; yytext[i] != '\0'; ++i )
    {
        if( yytext[i] == '\n' )
            column = 0;
        else if( yytext[i] == '\t' )
            column += 8 - ( column % 8 );
        else
            ++column;
    }

}

int yyerror( const char* msg, ... )
{
	printf( "%d:%d: %s\n", yylineno, column, msg );
	exit(1);
}
