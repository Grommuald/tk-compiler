#include "LabelStack.hpp"

using namespace std;

LabelStack& LabelStack::Instance()
{
    static LabelStack myInstance;
    return myInstance;
}

LabelStack::LabelStack()
{
}

LabelStack::~LabelStack()
{
}

void LabelStack::pop( uint32_t number )
{
    while ( m_labelStack.empty() == false && number-- )
        m_labelStack.pop();
}

void LabelStack::push()
{
    m_labelStack.push( "L" + to_string( m_currentLabelNumber ) );
    ++m_currentLabelNumber;
}

void LabelStack::pushSaved()
{
    m_labelStack.push( m_savedLabel );
}

void LabelStack::saveTop()
{
    m_savedLabel = m_labelStack.top();
}

string LabelStack::getSaved() const
{
    return m_savedLabel;
}

string LabelStack::getTop() const
{
    return m_labelStack.top();
}

void LabelStack::evaluateActionExpression( const bool& val )
{
    m_isActionExpressionUsed = val;
}

bool LabelStack::isActionExpressionUsed() const
{
    return m_isActionExpressionUsed;
}
LabelStack& labelStack = LabelStack::Instance();
