#include "Symbolic.hpp"

Symbol::Symbol( const TypeSpecifier& specifierType, const SymbolType& symbolType )
    : m_typeSpecifier ( specifierType ), m_symbolType( symbolType )
{
    if( specifierType == TypeSpecifier::Address )
    {
        m_suffix = new AddressSuffix();
    }
    else 
    {
        switch( symbolType )
        {
            case SymbolType::Function:
                m_suffix = new FunctionSuffix();
                break;

            case SymbolType::Array:
                m_suffix = new ArraySuffix(); 
                break;

            case SymbolType::Address:
                m_suffix = new AddressSuffix();
                break;

            default:
                m_suffix = nullptr;
        }
    }
}

Symbol::~Symbol()
{
    if ( m_suffix )
        delete m_suffix;
}

TypeSuffix::TypeSuffix() {}
TypeSuffix::~TypeSuffix() {}

void FunctionSuffix::pushParam( const std::string& id )
{
    m_paramsIds.push_back( id );
}

std::string SymbolGetter::getId( const idSymbolPair& pair )
{
    return pair.first;
}

Symbol* SymbolGetter::getSymbol( const idSymbolPair& pair )
{
    return pair.second;
}

ArraySuffix* SymbolGetter::getArraySuffixIfPossible( const idSymbolPair& pair )
{
    return dynamic_cast<ArraySuffix*>( pair.second->m_suffix ); 
}

FunctionSuffix* SymbolGetter::getFunctionSuffixIfPossible( const idSymbolPair& pair )
{
    return dynamic_cast<FunctionSuffix*>( pair.second->m_suffix ); 
}

AddressSuffix* SymbolGetter::getAddressSuffixIfPossible( const idSymbolPair& pair )
{
    return dynamic_cast<AddressSuffix*>( pair.second->m_suffix ); 
}

AddressSuffix* SymbolGetter::getAddressSuffixIfPossible( Symbol* symbol )
{
    return dynamic_cast<AddressSuffix*>( symbol->m_suffix ); 
}

TypeSpecifier SymbolGetter::getType( const idSymbolPair& pair )
{
    return pair.second->m_typeSpecifier;
}

SymbolType SymbolGetter::getSpecial( const idSymbolPair& pair )
{
    return pair.second->m_symbolType;
}

bool SymbolGetter::isReal( const idSymbolPair& pair )
{
    return getType( pair ) == TypeSpecifier::Real;
}

bool SymbolGetter::isAddress( const idSymbolPair& pair )
{
    return getType( pair ) == TypeSpecifier::Address;
}

bool SymbolGetter::isInteger( const idSymbolPair& pair )
{
    return getType( pair ) == TypeSpecifier::Integer;
}

bool SymbolGetter::isString( const idSymbolPair& pair )
{
    return getType( pair ) == TypeSpecifier::String;
}

bool SymbolGetter::isScalar( const idSymbolPair& pair )
{
    return getSpecial( pair ) == SymbolType::Variable;
}

bool SymbolGetter::isArray( const idSymbolPair& pair )
{
    return getSpecial( pair ) == SymbolType::Array;
}