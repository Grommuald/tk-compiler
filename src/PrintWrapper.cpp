#include "PrintWrapper.hpp"
#include "OperandStack.hpp"
#include "Debug.hpp"
#include <algorithm>

extern OperandStack& operandStack;
extern "C" int yyerror( const char* msg, ... );

using namespace std;

PrintWrapper& PrintWrapper::Instance()
{
    static PrintWrapper myInstance;
    return myInstance;
}

PrintWrapper::PrintWrapper()
{

}

PrintWrapper::~PrintWrapper()
{

}

void PrintWrapper::splitInclusionsString( const std::string& inclusionsString )
{
    string currentToken = "";
    bool skipSymbol = false;

    for( uint32_t i = 0; i < inclusionsString.length(); ++i )
    {
        if( skipSymbol == false )
        {
            if( inclusionsString[i] != '%' )
            {
                currentToken.push_back( inclusionsString[i] );
            }
            else
            {
                m_inclusionsStringParts.push_back( currentToken );
                currentToken = "%";

                if( inclusionsString[i+1] == '%' ) {
                    currentToken.push_back( inclusionsString[i] );
                }
                else if( inclusionsString[i+1] != 'd' && inclusionsString[i+1] != 'f' ) {
                    string message = "unrecognized token: %" + inclusionsString[i+1];
                    yyerror( message.c_str() );
                }
                else {
                    if( i+1 < inclusionsString.length() )
                    currentToken.push_back( inclusionsString[i+1] );

#                   if DEBUG_MODE == 1
                    cout << "Pushing string token: " << currentToken << endl;
#                   endif

                    m_inclusionsStringParts.push_back( currentToken );
                    currentToken = "";

                    
                }
                skipSymbol = true;
            }
        }
        else
            skipSymbol = false;
    }
    if( currentToken != "" )
        m_inclusionsStringParts.push_back( currentToken );
}

void PrintWrapper::printChain()
{
    bool isLastSymbol = false;
    uint32_t numberOfInclusions = 0;

    for( const auto& i : m_inclusionsStringParts )
    {
        if( i.find( "%" ) == 0 )
            ++numberOfInclusions;
    }
    
    if( numberOfInclusions != m_passedSymbols.size() )
    {
#       if DEBUG_MODE == 1
        cout << "numberOfInclusions: " << numberOfInclusions << ", " << "passedSymbols.size(): " << m_passedSymbols.size() << endl;
#       endif   
        yyerror( "number of arguments does not match number of inclusions" );
    }

#   if DEBUG_MODE == 1        
    for( auto i : m_inclusionsStringParts )
    {
        cout << "inclusionStringPart: " << i << endl;
    }
#   endif

    for( auto it = m_inclusionsStringParts.begin(); it != m_inclusionsStringParts.end(); ++it )
    {
        if( it == --m_inclusionsStringParts.end() )
        {
            isLastSymbol = true;
        }

        auto current = *it;

        if( current == "%d" || current == "%f" )
        {
            operandStack.push( move( m_passedSymbols.front() ) );

            if( current == "%d" )
                operandStack.pop(
                    OperationType::Unary, isLastSymbol && m_isPrintln ? "printiln" : "printi" );
            else
                operandStack.pop(
                    OperationType::Unary, isLastSymbol && m_isPrintln ? "printrln" : "printr" );

            if( m_passedSymbols.empty() == false )
                m_passedSymbols.erase( m_passedSymbols.begin() );
        }
        else
        {
            if( isLastSymbol || current != "\"" && current.empty() == false )
            {
                operandStack.push( current );
                operandStack.pop(
                    OperationType::Unary, isLastSymbol && m_isPrintln ? "println" : "print" );
            }
        }
    }

    m_inclusionsStringParts.clear();
    m_isPrintln = false;
}

void PrintWrapper::passToken( idSymbolPair token )
{
    m_passedSymbols.push_back( token );
}

void PrintWrapper::setIsExtendedPrintln()
{
    m_isPrintln = true;
}

PrintWrapper& printWrapper = PrintWrapper::Instance();
