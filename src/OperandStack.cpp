#include "OperandStack.hpp"
#include "SymbolTab.hpp"
#include "CodeGenerator.hpp"
#include "Debug.hpp"
#include "TriplesLogger.hpp"

#include <algorithm>
#include <cassert>

using namespace std;

extern SymbolTab& symbolTab;
extern CodeGenerator& codeGenerator;
extern "C" int yyerror( const char* msg, ... );

OperandStack& OperandStack::Instance()
{
    static OperandStack myInstance;
    return myInstance;
}

OperandStack::OperandStack()
{

}

OperandStack::~OperandStack()
{

}

void OperandStack::push( const int& val )
{
    DebugLogger::log( __FUNCTION__, "entering (int)" );

    m_stack.emplace(
        make_pair( to_string( val ), new Symbol( TypeSpecifier::Integer ) )
    );
}

void OperandStack::push( const float& val )
{
    DebugLogger::log( __FUNCTION__, "entering (float)" );

    if( m_floatsLiteralsMap.find( val ) == m_floatsLiteralsMap.end() )
    {
        m_floatsLiteralsMap[val] = "__f" + to_string( m_floatLiteralsNameNumber++ );
        codeGenerator.pushData(
            make_pair( m_floatsLiteralsMap[val], new Symbol( TypeSpecifier::Real ) ),
            val
        );
    }
    m_stack.emplace(
        make_pair( m_floatsLiteralsMap[val], new Symbol( TypeSpecifier::Real ) )
    );
}

void OperandStack::push( const string& val )
{
    DebugLogger::log( __FUNCTION__, "entering (string)" );

    if( m_stringLiteralsMap.find( val ) == m_stringLiteralsMap.end() )
    {
        m_stringLiteralsMap[val] = "__str" + to_string( m_stringLiteralsNameNumber++ );
        codeGenerator.pushData(
            make_pair( m_stringLiteralsMap[val], new Symbol( TypeSpecifier::String ) ),
            val
        );
    }
    m_stack.emplace(
        make_pair( m_stringLiteralsMap[val], new Symbol( TypeSpecifier::String ) )
    );
}

void OperandStack::push( idSymbolPair&& pair )
{
    DebugLogger::log( __FUNCTION__, "entering (&&)" );
    m_stack.emplace( pair );
}

void OperandStack::push( const idSymbolPair& pair )
{
    DebugLogger::log( __FUNCTION__, "entering (const &)" );
    m_stack.emplace( pair );
}

void OperandStack::pushInputInteger()
{
    DebugLogger::log( __FUNCTION__, "entering" );

    m_stack.emplace(
        make_pair( "__input", new Symbol( TypeSpecifier::Integer, SymbolType::Input ) )
    );
}

void OperandStack::pushInputReal()
{
    DebugLogger::log( __FUNCTION__, "entering" );

    m_stack.emplace(
        make_pair( "__input", new Symbol( TypeSpecifier::Real, SymbolType::Input ) )
    );
}

void OperandStack::pushNewId( const string& id )
{
    DebugLogger::log( __FUNCTION__, "entering" );

    symbolTab.pushId( id );
    m_stack.emplace(
        make_pair( id, symbolTab.get( id ) )
    );
}

void OperandStack::pushNewId()
{
    DebugLogger::log( __FUNCTION__, "entering" );

    string id = symbolTab.getIdOfIncomingSymbol();

    symbolTab.pushId( id );
    m_stack.emplace(
        make_pair( id, symbolTab.get( id ) )
    );
}

void OperandStack::pushExistingId( const string& id )
{
    DebugLogger::log( __FUNCTION__, "entering" );

    if ( symbolTab.exists( id ) )
    {
        m_stack.emplace(
            make_pair( id, symbolTab.get( id ) )
        );
    }
    else
    {
        std::string message = "Undeclared identifier: " + id;
        yyerror( message.c_str() );
    }
}

void OperandStack::pushPrefixOperator( const std::string& _operator )
{
    DebugLogger::log( __FUNCTION__, "entering" );

    m_incomingPrefixOperators.push( _operator );
}

uint8_t OperandStack::getOperandsNumber( const OperationType& type ) const
{
    DebugLogger::log( __FUNCTION__, "entering" );

    switch( type )
    {
        case OperationType::Unary:
            return 1;

        case OperationType::Binary:
            return 2;

        default:
            return 0;
    }
}

bool OperandStack::isTemporaryVariableAvailable( const TypeSpecifier& type )
{
    DebugLogger::log( __FUNCTION__, "entering" );

    for( const auto& i : m_temporaryVariablesMap )
    {
        if( i.second.second && i.second.first == type )
        {
            m_designatedAvailableTempId = i.first;

            DebugLogger::log( 
                __FUNCTION__, 
                "isTemporatyVariableAvailable: chosen id: %s", 
                m_designatedAvailableTempId.c_str() );
            return true;
        }
    }
    return false;
}

bool OperandStack::isTemporaryAddressAvailable( const TypeSpecifier& type )
{
    DebugLogger::log( __FUNCTION__, "entering" );

    for( const auto& i : m_temporaryAddressesMap )
    {
        if( i.second.second && i.second.first == type )
        {
            DebugLogger::log( __FUNCTION__, "designatedAvailableId: %s", i.first.c_str() );
            m_designatedAvailableTempId = i.first;
            return true;
        }
    }
    return false;
}

std::string OperandStack::getAvailableTemporaryVariableId() const
{
    return m_designatedAvailableTempId;
}

void OperandStack::setNewIncomingTypeBasedOnOperandsType(
    const vector<idSymbolPair>& operands
)
{
    DebugLogger::log( __FUNCTION__, "entering" );

    if( any_of( operands.begin(), operands.end(),
        [] ( const idSymbolPair& p )
        {
            return SymbolGetter::isString( p );
        }
    ) )
    {
        DebugLogger::log( __FUNCTION__, "setTypeOfIncomingId: string" );
        symbolTab.setTypeOfIncomingId( TypeSpecifier::String );
    }
    else if( any_of( operands.begin(), operands.end(),
        [] ( const idSymbolPair& p )
        {
            return SymbolGetter::isArray( p );
        }
    ) )
    {
        auto p = find_if( operands.begin(), operands.end(),
        [] ( const idSymbolPair& p )
        {
            return SymbolGetter::isArray( p );
        } );

        DebugLogger::log( __FUNCTION__, "setTypeOfIncomingId: address" );
        symbolTab.setTypeOfIncomingId( TypeSpecifier::Address );
        
        switch( SymbolGetter::getType( *p ) )
        {
            case TypeSpecifier::Integer:
                DebugLogger::log( __FUNCTION__, "setTypeOfIncomingId: int" );
                symbolTab.setAddressTypeOfIncomingId( TypeSpecifier::Integer );
                break;

            case TypeSpecifier::Real:
                DebugLogger::log( __FUNCTION__, "setTypeOfIncomingId: real" );
                symbolTab.setAddressTypeOfIncomingId( TypeSpecifier::Real );
                break;
        }
    }
    else if( any_of( operands.begin(), operands.end(),
        [] ( const idSymbolPair& p )
        {
            return SymbolGetter::isReal( p );
        }
    ) )
    {
        DebugLogger::log( __FUNCTION__, "setTypeOfIncomingId: real" );
        symbolTab.setTypeOfIncomingId( TypeSpecifier::Real );
    }
    else
    {
        DebugLogger::log( __FUNCTION__, "setTypeOfIncomingId: int" );
        symbolTab.setTypeOfIncomingId( TypeSpecifier::Integer );
    }
}

void OperandStack::checkAndPushId(
    const TypeSpecifier& typeSpecifier,
    const SymbolType& symbolType
)
{
    DebugLogger::log( __FUNCTION__, "entering" );

    if( 
        ( symbolType == SymbolType::Variable && isTemporaryVariableAvailable( typeSpecifier ) )
     || ( symbolType == SymbolType::Address && isTemporaryAddressAvailable( typeSpecifier ) ) )
    {
        string availableId = getAvailableTemporaryVariableId();
        m_temporaryIdsStack.push_back( availableId );

        DebugLogger::log( __FUNCTION__, "__tempRes available: %s", availableId.c_str() );

        if( symbolType == SymbolType::Variable )
        {
             m_temporaryVariablesMap[availableId].second = false;
        }  
        else if( symbolType == SymbolType::Address )
        {
            SymbolGetter::getAddressSuffixIfPossible( symbolTab.get( availableId ) )->m_rootArray 
                = m_incomingArraySuffix;
            SymbolGetter::getAddressSuffixIfPossible( symbolTab.get( availableId ) )->m_pointedArrayId
                = m_incomingArrayId;

            m_temporaryAddressesMap[availableId].second = false;
        }
    }
    else
    {
        DebugLogger::log( __FUNCTION__, "__tempRes NOT available" );
        pushNewTemporaryVariable();
    }
}

void OperandStack::pushNewTemporaryVariable()
{
    DebugLogger::log( __FUNCTION__, "entering" );

    string newTempName = "";
    if( symbolTab.getTypeOfIncomingId() == TypeSpecifier::Address )
    {
        newTempName = "__tempAddr" + to_string( m_temporaryAddressNameNumber++ );
        m_temporaryIdsStack.push_back( newTempName );

        DebugLogger::log( __FUNCTION__, "creating %s", newTempName.c_str() );

        m_temporaryAddressesMap[newTempName] = make_pair(
            symbolTab.getAddressTypeOfIncomingId(),
            false
        );
        symbolTab.setSuffixOfIncomingId( SymbolType::Address );
    }
    else
    {
        newTempName = "__tempRes" + to_string( m_temporaryVariableNameNumber++ );
        m_temporaryIdsStack.push_back( newTempName );

        DebugLogger::log( __FUNCTION__, "creating %s", newTempName.c_str() );

        m_temporaryVariablesMap[newTempName] = make_pair(
            symbolTab.getTypeOfIncomingId(),
            false
        );
        symbolTab.setSuffixOfIncomingId( SymbolType::Variable );
    }
   
    symbolTab.pushId( newTempName );

    // set address type of new tempAddressVar's suffix
    if( symbolTab.getTypeOfIncomingId() == TypeSpecifier::Address )
    {
        assert( SymbolGetter::getAddressSuffixIfPossible( symbolTab.get( newTempName ) ) );

        SymbolGetter::getAddressSuffixIfPossible( symbolTab.get( newTempName ) )->m_pointedType 
            = symbolTab.getAddressTypeOfIncomingId();
        SymbolGetter::getAddressSuffixIfPossible( symbolTab.get( newTempName ) )->m_rootArray 
            = m_incomingArraySuffix;
        SymbolGetter::getAddressSuffixIfPossible( symbolTab.get( newTempName ) )->m_pointedArrayId
            = m_incomingArrayId;
    }
}

void OperandStack::setArraySuffixForIncomingAddress( const idSymbolPair& arrayOperand )
{
    DebugLogger::log( __FUNCTION__, "entering" );

    m_incomingArraySuffix = SymbolGetter::getArraySuffixIfPossible( arrayOperand );
}

void OperandStack::setArrayIdForIncomingAddress( const idSymbolPair& arrayOperand )
{
    DebugLogger::log( __FUNCTION__, "entering" );

    m_incomingArrayId = SymbolGetter::getId( arrayOperand );
}

bool OperandStack::checkIfNewAddressVariableIsNeeded( 
    idSymbolPair& arrayOperand,
    const std::string& op )
{
    DebugLogger::log( __FUNCTION__, "entering" );

    if( op == "[]" )
    {
        if( SymbolGetter::isAddress( arrayOperand ) || SymbolGetter::isArray( arrayOperand ) )
        {
            if( SymbolGetter::isArray( arrayOperand ) )
            {
                setNewIncomingTypeBasedOnOperandsType( { arrayOperand } );
                setArraySuffixForIncomingAddress( arrayOperand );
                setArrayIdForIncomingAddress( arrayOperand );
                checkAndPushId( symbolTab.getAddressTypeOfIncomingId(), SymbolType::Address );
                pushExistingId( m_temporaryIdsStack.back() );

                arrayOperand = make_pair( m_temporaryIdsStack.back(), symbolTab.get( m_temporaryIdsStack.back() ) );
                codeGenerator.zeroAddress( arrayOperand );
            }
            else if( SymbolGetter::getAddressSuffixIfPossible( arrayOperand )->m_rootArray == nullptr )
            {
                AddressSuffix* addr = SymbolGetter::getAddressSuffixIfPossible( arrayOperand );

                DebugLogger::log( __FUNCTION__, "stand-alone pointer" );

                // stand-alone pointer
                DebugLogger::log( __FUNCTION__, "setTypeOfIncomingId: address" );
                symbolTab.setTypeOfIncomingId( TypeSpecifier::Address );
                symbolTab.setAddressTypeOfIncomingId( addr->m_pointedType );
    
                setArrayIdForIncomingAddress( arrayOperand );
                checkAndPushId( symbolTab.getAddressTypeOfIncomingId(), SymbolType::Address );
                pushExistingId( m_temporaryIdsStack.back() );

                arrayOperand = make_pair( m_temporaryIdsStack.back(), symbolTab.get( m_temporaryIdsStack.back() ) );
                codeGenerator.zeroAddress( arrayOperand );
            }
            else
            {
                pushExistingId( SymbolGetter::getId( arrayOperand ) );
            }
        }
        else 
        {
            string message = SymbolGetter::getId( arrayOperand ) + " must be an array or an address.";
            yyerror( message.c_str() );
        }
        return true;
    }
    return false;
}

void OperandStack::checkIfNewTemporaryVariableIsNeeded(
    vector<idSymbolPair>& operands,
    const string& op
)
{
    DebugLogger::log( __FUNCTION__, "entering" );

    auto isBareAssignment =
    [this, op]
    {
        return op == "=" && m_stack.size() == 1;
    };

    if( all_of( operands.begin(), operands.end(),
        [] ( const idSymbolPair& id )
        {
            return id.first.find( "__tempRes" ) != 0;
        } ) 
    &&  isBareAssignment() == false )
    {
        DebugLogger::log( __FUNCTION__, "needed" );
        setNewIncomingTypeBasedOnOperandsType( operands );
        checkAndPushId( symbolTab.getTypeOfIncomingId() );
    }
    
    if( isBareAssignment() == false )
        pushExistingId( m_temporaryIdsStack.back() );
}

void OperandStack::printTemporaryIdsStack()
{
    DebugLogger::log( __FUNCTION__, "entering" );

    for( const auto& i : m_temporaryIdsStack ) 
    {
        DebugLogger::log( __FUNCTION__, "\t%s", i.c_str() );
    }
}

/*
   If both the operands are __tempRes then we can pass the result of the operation
   to only one of them and unlock the other one
*/

void OperandStack::tryToUnlockTemporaryVariable(
    vector<idSymbolPair>& operands
)
{
    DebugLogger::log( __FUNCTION__, "entering" );

    if ( all_of( operands.begin(), operands.end(),
    [=] ( const idSymbolPair& p )
    {
        return p.first.find( "__tempRes" ) == 0;
    }
    ) && operands.size() == 2 )
    {
        if( SymbolGetter::getType( operands[0] ) != SymbolGetter::getType( operands[1] ) )
        {
            if( SymbolGetter::isReal( operands[0] ) )
            {
                DebugLogger::log( __FUNCTION__, "(lhs, rhs): (real, int)" );
                if( m_temporaryIdsStack.back() != SymbolGetter::getId( operands[0] ) )
                {
                    DebugLogger::log( __FUNCTION__, "int, real -> real" );
                    m_temporaryIdsStack.pop_back();
                }
                else
                {
                    DebugLogger::log( __FUNCTION__, "real, int -> real" );
                    string topId = m_temporaryIdsStack.back();
                    m_temporaryIdsStack.pop_back();
                    m_temporaryIdsStack.pop_back();
                    m_temporaryIdsStack.push_back( topId );
                }
                m_temporaryVariablesMap[SymbolGetter::getId( operands[1] )].second = true;
            }
            else
            {
                DebugLogger::log( __FUNCTION__, "(lhs, rhs): (int, real)" );
                if( m_temporaryIdsStack.back() != SymbolGetter::getId( operands[1] ) )
                {
                    DebugLogger::log( __FUNCTION__, "int, real -> real" );
                    m_temporaryIdsStack.pop_back();
                }
                else
                {
                    DebugLogger::log( __FUNCTION__, "real, int -> real" );
                    string topId = m_temporaryIdsStack.back();
                    m_temporaryIdsStack.pop_back();
                    m_temporaryIdsStack.push_back( topId );
                }
                m_temporaryVariablesMap[SymbolGetter::getId( operands[0] )].second = true;
            }
        }
        else
        {
            DebugLogger::log( __FUNCTION__, "setting %s back to true", m_temporaryIdsStack.back().c_str() );

            m_temporaryVariablesMap[m_temporaryIdsStack.back()].second = true;
            m_temporaryIdsStack.pop_back();
        }
    }
}

/*
   For situations like:
   int i = 2 + 3 + 2.0;

   it allocates __temp0 of type of an integer then
   saves the sum of 2 and 3 in it and finally
   has to find another real __temp to save the sum
   of __temp0 and real value.
*/

void OperandStack::loadInputSymbols(
    idSymbolPair& operand
)
{
    DebugLogger::log( __FUNCTION__, "entering" );
    if( SymbolGetter::getSpecial( operand ) == SymbolType::Input )
    {
        DebugLogger::log( __FUNCTION__, "loading input symbol" );
        switch( SymbolGetter::getType( operand ) )
        {
            case TypeSpecifier::Integer:
                symbolTab.setTypeOfIncomingId( TypeSpecifier::Integer );
                checkAndPushId( TypeSpecifier::Integer );
                break;

            case TypeSpecifier::Real:
                symbolTab.setTypeOfIncomingId( TypeSpecifier::Real );
                checkAndPushId( TypeSpecifier::Real );
                break;
        }

        operand = make_pair( m_temporaryIdsStack.back(), symbolTab.get( m_temporaryIdsStack.back() ) );
        codeGenerator.loadInputTo( operand );
    }
}

void OperandStack::loadInputSymbols(
    std::vector<idSymbolPair>& operands
)
{
    DebugLogger::log( __FUNCTION__, "entering" );

    for( auto& i : operands )
    {
        if( SymbolGetter::getSpecial( i ) == SymbolType::Input )
        {
            DebugLogger::log( __FUNCTION__, "loading input symbol" );
            switch( SymbolGetter::getType( i ) )
            {
                case TypeSpecifier::Integer:
                    symbolTab.setTypeOfIncomingId( TypeSpecifier::Integer );
                    checkAndPushId( TypeSpecifier::Integer );
                    break;

                case TypeSpecifier::Real:
                    symbolTab.setTypeOfIncomingId( TypeSpecifier::Real );
                    checkAndPushId( TypeSpecifier::Real );
                    break;
            }

            i = make_pair( m_temporaryIdsStack.back(), symbolTab.get( m_temporaryIdsStack.back() ) );
            codeGenerator.loadInputTo( i );
        }
    }
}

void OperandStack::dereferenceAddressesIfNeeded(
    std::vector<idSymbolPair>& operands,
    const std::string& op
)
{
    DebugLogger::log( __FUNCTION__, "entering" );

    if( op == "=" )
    {
        if( SymbolGetter::getAddressSuffixIfPossible( operands[1] ) )
        {
            if( SymbolGetter::getAddressSuffixIfPossible( operands[0] ) )
            {
                if( SymbolGetter::getAddressSuffixIfPossible( operands[0] )->m_isReadyToDereference == false 
                    && SymbolGetter::getAddressSuffixIfPossible( operands[1] )->m_isReadyToDereference == false )
                {
                    // addr = addr;
                    // skip
                    return;
                }
                else if( SymbolGetter::getAddressSuffixIfPossible( operands[0] )->m_isReadyToDereference == false 
                    && SymbolGetter::getAddressSuffixIfPossible( operands[1] )->m_isReadyToDereference == true )
                {
                    // addr[] = addr;
                    // error
                    yyerror( "Cannot assign a non-dereferenced address to a dereferenced one" );
                }
                else if( SymbolGetter::getAddressSuffixIfPossible( operands[0] )->m_isReadyToDereference == true 
                    && SymbolGetter::getAddressSuffixIfPossible( operands[1] )->m_isReadyToDereference == false )
                {
                    // addr = addr[];
                    yyerror( "Cannot assign a dereferenced address to a non-dereferenced one" );
                }
            }
            else
            {
                if( SymbolGetter::getAddressSuffixIfPossible( operands[1] )->m_isReadyToDereference == false )
                {
                    // addr = non-addr
                    yyerror( "Cannot assign a non-address to an address." );
                }
            }
        }
    }
   
  
    if( op == "=" && SymbolGetter::isAddress( operands[1] ) )
    {
        DebugLogger::log( __FUNCTION__, "address = value -> skip lhs || address = address -> dereference rhs" );
        if( SymbolGetter::isAddress( operands[0] ) )
        {
            TypeSpecifier type = SymbolGetter::getAddressSuffixIfPossible( operands[0] )->m_pointedType;
    
            symbolTab.setTypeOfIncomingId( type );
            checkAndPushId( type );

            auto castedSymbol =
                make_pair( m_temporaryIdsStack.back(), symbolTab.get( m_temporaryIdsStack.back() ) );

            m_temporaryIdsStack.pop_back();

            if( type == TypeSpecifier::Integer )
                codeGenerator.dereferenceToInt( operands[0], castedSymbol );
            else if( type == TypeSpecifier::Real )
                codeGenerator.dereferenceToFloat( operands[0], castedSymbol );

            DebugLogger::log( __FUNCTION__, "top(): %s", m_temporaryIdsStack.back().c_str() );

            auto addrSuff = SymbolGetter::getAddressSuffixIfPossible( operands[0] );

            addrSuff->m_isReadyToDereference = false;
            addrSuff->m_currentNestLevel = 0;
            addrSuff->m_pointedArrayId = "";
            addrSuff->m_rootArray = nullptr;
            
            m_temporaryAddressesMap[SymbolGetter::getId( operands[0] ) ].second = true;
            operands[0] = castedSymbol;
        }
    }
    else
    {
        DebugLogger::log( __FUNCTION__, "variable OP value || variable OP address" );
        if( m_isTempAddrNeededToBeRemoved )
        {
            DebugLogger::log( __FUNCTION__, "Entering redundant tempAddrId remover..." );
            m_temporaryIdsStack.erase(
                remove_if( m_temporaryIdsStack.begin(), 
                            m_temporaryIdsStack.end(),
                            [&](const std::string& id )
                            {
                                return id == SymbolGetter::getId( operands[0] );
                            }),
                            m_temporaryIdsStack.end()
                    );

            m_isTempAddrNeededToBeRemoved = false;
        }

        for( auto& i : operands )
        {
            DebugLogger::log( __FUNCTION__, "i: %s", SymbolGetter::getId( i ).c_str() );
            if( SymbolGetter::isAddress( i ) && SymbolGetter::getAddressSuffixIfPossible( i )->m_isReadyToDereference )
            {
                DebugLogger::log( __FUNCTION__, "found an address variable" );

                if( m_temporaryIdsStack.back() == SymbolGetter::getId( i ) )
                    m_temporaryIdsStack.pop_back();
                    
                TypeSpecifier type = SymbolGetter::getAddressSuffixIfPossible( i )->m_pointedType;

                symbolTab.setTypeOfIncomingId( type );
                checkAndPushId( type );

                DebugLogger::log( __FUNCTION__, "pushed __tempRes: %s", m_temporaryIdsStack.back().c_str() );

                auto castedSymbol =
                    make_pair( m_temporaryIdsStack.back(), symbolTab.get( m_temporaryIdsStack.back() ) );

                m_temporaryIdsStack.pop_back();
                printTemporaryIdsStack();

                if( type == TypeSpecifier::Integer )
                    codeGenerator.dereferenceToInt( i, castedSymbol );
                else if( type == TypeSpecifier::Real )
                    codeGenerator.dereferenceToFloat( i, castedSymbol );

                auto addrSuff = SymbolGetter::getAddressSuffixIfPossible( i );

                addrSuff->m_isReadyToDereference = false;
                addrSuff->m_currentNestLevel = 0;
                addrSuff->m_pointedArrayId = "";
                addrSuff->m_rootArray = nullptr;

                m_temporaryAddressesMap[SymbolGetter::getId( i ) ].second = true;
                i = castedSymbol;
            }
        }
    }
}

void OperandStack::doTypeCastingIfNeeded(
    std::vector<idSymbolPair>& operands,
    const std::string& op,
    const OperationType& operationType
)
{
    DebugLogger::log( __FUNCTION__, "entering" );

    auto intOperandIterator = find_if( operands.begin(), operands.end(),
    [] ( const idSymbolPair& pair )
    {
        return SymbolGetter::isInteger( pair );
    } );

    auto realOperandIterator = find_if( operands.begin(), operands.end(),
    [] ( const idSymbolPair& pair )
    {
        return SymbolGetter::isReal( pair );
    } );

    auto castIfNeeded =
    [&] (
        decltype( intOperandIterator )& iterator,
        const TypeSpecifier& type,
        const bool& setIncomingType = true )
    {
        DebugLogger::log( __FUNCTION__, "casting needed" );
        if( iterator != operands.end() )
        {
            if( setIncomingType )
                symbolTab.setTypeOfIncomingId( type );

            checkAndPushId( type );

            auto castedSymbol =
                make_pair( m_temporaryIdsStack.back(), symbolTab.get( m_temporaryIdsStack.back() ) );

            m_temporaryIdsStack.pop_back();

            if( type == TypeSpecifier::Integer )
                codeGenerator.castToInt( *iterator, castedSymbol );
            else if( type == TypeSpecifier::Real )
                codeGenerator.castToFloat( *iterator, castedSymbol );

            *iterator = castedSymbol;
        }
    };

    if( op == "printi" || op == "printiln" )
    {
        castIfNeeded( realOperandIterator, TypeSpecifier::Integer );
    }
    else if( op == "printr" || op == "printrln" )
    {
        castIfNeeded( intOperandIterator, TypeSpecifier::Real );
    }
    else if( op != "=" && op != "[]"
        && intOperandIterator != operands.end()
        && realOperandIterator != operands.end() )
    {
        castIfNeeded( intOperandIterator, TypeSpecifier::Real, false );
    }
}

vector<idSymbolPair> OperandStack::popNext( 
    const uint32_t& numberOfOperands, 
    const OperationType& operationType
)
{
    vector<idSymbolPair> currentSymbols;
    uint8_t operandsNumber = getOperandsNumber( operationType );

    while( operandsNumber-- )
    {
        TriplesLogger::pushToken( 
            SymbolGetter::getId( m_stack.top() ) 
        );

        currentSymbols.push_back( m_stack.top() );
        m_stack.pop();
    }

    DebugLogger::log( __FUNCTION__, "incoming operands: " );
    for( const auto& i : currentSymbols )
    {
        DebugLogger::log( __FUNCTION__, "\t%s", SymbolGetter::getId( i ).c_str() );
        if( SymbolGetter::isAddress( i ) )
            DebugLogger::log( "", "\t\tand is an address" );
    }

    return currentSymbols;
}

void OperandStack::pop( const OperationType& operationType, const std::string& _operator )
{
    DebugLogger::log( __FUNCTION__, "entering" );
    DebugLogger::log( __FUNCTION__, "operator: %s", _operator.c_str() );

    printTemporaryIdsStack();

    auto currentSymbols = popNext( getOperandsNumber( operationType ), operationType );

    TriplesLogger::pushToken( _operator );
    TriplesLogger::pushNewLine();
    
    if( _operator == "=" )
    {
        if( SymbolGetter::getId( currentSymbols[1] ).find( "__tempRes" ) == 0 
            || SymbolGetter::getSpecial( currentSymbols[1] ) == SymbolType::Literal ) 
        {
            yyerror( "Incorrect l-value." );
        } 
    }
    
    if( all_of( currentSymbols.begin(), currentSymbols.end(),
    [] ( const idSymbolPair& pair )
    {
        return SymbolGetter::isString( pair ) == false;
    } ) )
    {
        loadInputSymbols( currentSymbols );
        if( operationType == OperationType::Binary )
        {
            tryToUnlockTemporaryVariable( currentSymbols );

            if( SymbolGetter::isArray( currentSymbols[1] ) && SymbolGetter::isAddress( currentSymbols[0] ) )
            {
                m_isTempAddrNeededToBeRemoved = true;
            }
        }
        
        if( checkIfNewAddressVariableIsNeeded( currentSymbols[1], _operator ) == false )
            checkIfNewTemporaryVariableIsNeeded( currentSymbols, _operator );
        
        dereferenceAddressesIfNeeded( currentSymbols, _operator );
        doTypeCastingIfNeeded( currentSymbols, _operator, operationType );
    }
    
    switch( operationType )
    {
        case OperationType::Unary:
            codeGenerator.pushUnaryOperation(
                _operator,
                currentSymbols[0],
                m_temporaryIdsStack.empty() ? "" : m_temporaryIdsStack.back()
            );
            break;

        case OperationType::Binary:
            codeGenerator.pushBinaryOperation(
                _operator,
                currentSymbols[0],
                currentSymbols[1],
                m_temporaryIdsStack.empty() ? "" : m_temporaryIdsStack.back()
            );
            break;
    }
}

void OperandStack::pop( const OperationType& operationType, const char& _operator )
{
    DebugLogger::log( __FUNCTION__, "entering" );
    pop( operationType, "" + _operator );
}

void OperandStack::pop( const OperationType& operationType )
{
    DebugLogger::log( __FUNCTION__, "entering" );
    pop( operationType, m_incomingPrefixOperators.top() );
    m_incomingPrefixOperators.pop();
}

void OperandStack::popAllocatedAddress()
{
    DebugLogger::log( __FUNCTION__, "entering" );

    idSymbolPair& sizeOperand = m_stack.top();
    m_stack.pop();

    DebugLogger::log( __FUNCTION__, "incoming operand: " );
    DebugLogger::log( __FUNCTION__, "   %s", SymbolGetter::getId( sizeOperand ).c_str() );

    loadInputSymbols( sizeOperand );

    if( SymbolGetter::getType( sizeOperand ) != TypeSpecifier::Integer )
    {
        yyerror( "new [ size ]: size must be an integer value" );
    }

    symbolTab.setAddressTypeOfIncomingId( symbolTab.getTypeOfIncomingId() );

    string tempId = "";

    if( isTemporaryAddressAvailable( symbolTab.getTypeOfIncomingId() ) )
    {
        tempId = getAvailableTemporaryVariableId();
        m_temporaryIdsStack.push_back( tempId );

        DebugLogger::log( __FUNCTION__, "__tempAddr available: %s", tempId.c_str() );
  
        SymbolGetter::getAddressSuffixIfPossible( symbolTab.get( tempId ) )->m_rootArray 
            = nullptr;
        SymbolGetter::getAddressSuffixIfPossible( symbolTab.get( tempId ) )->m_pointedArrayId
            = "";

        m_temporaryAddressesMap[tempId].second = false;

    }
    else
    {
        DebugLogger::log( __FUNCTION__, "__tempAddr NOT available" );

        tempId = "__tempAddr" + to_string( m_temporaryAddressNameNumber++ );
        m_temporaryIdsStack.push_back( tempId );

        DebugLogger::log( __FUNCTION__, "creating %s", tempId.c_str() );

        m_temporaryAddressesMap[tempId] = make_pair(
            symbolTab.getAddressTypeOfIncomingId(),
            false
        );
        symbolTab.setTypeOfIncomingId( TypeSpecifier::Address );
        symbolTab.setSuffixOfIncomingId( SymbolType::Address );
        symbolTab.pushId( tempId );

        SymbolGetter::getAddressSuffixIfPossible( symbolTab.get( tempId ) )->m_pointedType 
            = symbolTab.getAddressTypeOfIncomingId();
        SymbolGetter::getAddressSuffixIfPossible( symbolTab.get( tempId ) )->m_rootArray 
            = nullptr;
        SymbolGetter::getAddressSuffixIfPossible( symbolTab.get( tempId ) )->m_pointedArrayId
            = "";
    }

    codeGenerator.pushUnaryOperation(
        "new",
        sizeOperand,
        m_temporaryIdsStack.back()
    );

    pushExistingId( tempId );
}

void OperandStack::clear()
{
    DebugLogger::log( __FUNCTION__, "entering" );

    for( auto& i : m_temporaryVariablesMap )
        i.second.second = true;

    for( auto& i : m_temporaryAddressesMap )
    {
        DebugLogger::log( __FUNCTION__, "clearing: %s", i.first.c_str() );
        auto addrSuff = SymbolGetter::getAddressSuffixIfPossible( symbolTab.get( i.first ) );
    
        addrSuff->m_isReadyToDereference = false;
        addrSuff->m_currentNestLevel = 0;
        addrSuff->m_pointedArrayId = "";
        addrSuff->m_rootArray = nullptr;

        i.second.second = true;
    }
        
    m_stack = std::stack<idSymbolPair>();
    m_temporaryIdsStack = std::vector<string>();
}

OperandStack& operandStack = OperandStack::Instance();
