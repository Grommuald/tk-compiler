#include "ArrayPacker.hpp"
#include "CodeGenerator.hpp"

#include <algorithm>
#include <vector>
#include <iostream>
#include <utility>
#include <numeric>

using namespace std;

extern CodeGenerator& codeGenerator;
extern "C" int yyerror( const char* msg, ... );

ArrayPacker& ArrayPacker::Instance()
{
    static ArrayPacker myInstance;
    return myInstance;
}

ArrayPacker::ArrayPacker()
{

}

ArrayPacker::~ArrayPacker()
{
   
}

void ArrayPacker::addDimension( const uint32_t& size )
{
    m_dimensionSizes.push_back( size );
}

void ArrayPacker::setArraySizesOf( const idSymbolPair& symbol )
{
    if( ArraySuffix* suffix = SymbolGetter::getArraySuffixIfPossible( symbol ) )
    {
        if( m_dimensionSizes.size() > 3 )
        {
            yyerror( "Cannot operate on more than 3 dimensions." );
        }

        copy( 
            m_dimensionSizes.rbegin(), 
            m_dimensionSizes.rend(),
            std::back_inserter( suffix->m_dimensions )
        );

        suffix->m_numberOfBytesToAllocate 
            = accumulate( m_dimensionSizes.begin(), m_dimensionSizes.end(), 1, std::multiplies<int>() );

        uint8_t wordSize = 4;

        switch( SymbolGetter::getType( symbol ) )
        {
            case TypeSpecifier::Integer:
            case TypeSpecifier::Real:
                wordSize = 4;
                break;
        }
        suffix->m_numberOfBytesToAllocate *= wordSize;
        m_dimensionSizes.clear();
    }
}

ArrayPacker& arrayPacker = ArrayPacker::Instance();
