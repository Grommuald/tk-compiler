#include "TriplesLogger.hpp"
using namespace std;

std::ofstream TriplesLogger::outputFile;
std::string TriplesLogger::outputFileName = "triples.txt";
bool TriplesLogger::enabled = false;

void TriplesLogger::setEnabled()
{
    outputFile.open( outputFileName );
    enabled = true;
}

void TriplesLogger::pushToken( const std::string& token )
{
    if( enabled )
        outputFile << token << " ";
}

void TriplesLogger::pushNewLine()
{
    if( enabled )
        outputFile << endl;
}


