%{
#include "OperandStack.hpp"
#include "LabelStack.hpp"
#include "SymbolTab.hpp"
#include "CodeGenerator.hpp"
#include "PrintWrapper.hpp"
#include "ArrayPacker.hpp"
#include "TriplesLogger.hpp"
#include "Debug.hpp"

#include <string>
#include <cstdio>
#include <iostream>
#include <unistd.h>

#define INFILE_ERROR    1
#define OUTFILE_ERROR   2
#define YYDEBUG 0

extern FILE* yyin;
extern FILE* yyout;

extern "C" int yylex();
extern "C" int yyerror( const char* msg, ... );

extern CodeGenerator&   codeGenerator;
extern OperandStack&    operandStack;
extern LabelStack&      labelStack;
extern SymbolTab&       symbolTab;
extern PrintWrapper&    printWrapper;
extern ArrayPacker&     arrayPacker;

using namespace std;
%}
%locations

%union
{
    int ival;
    float fval;
    char* sval;
}

%token <ival> INT_LITERAL
%token <fval> REAL_LITERAL
%token <sval> STRING_LITERAL

%token <sval> IDENTIFIER

%token <sval> TRUE
%token <sval> FALSE

%token <sval> INT
%token <sval> REAL

%token WHILE FOR IF ELSE RETURN BREAK CONTINUE NEW
%token PRINT PRINTI PRINTR PRINTLN PRINTILN PRINTRLN INPUTI INPUTR

%token INC_OP DEC_OP

%token OR_OP AND_OP EQ_OP NE_OP
%token GE_OP LE_OP

%left '+' '-' '*' '/' '%'
%left OR_OP AND_OP EQ_OP GE_OP LE_OP
%right '=' '!'

%start translation_unit

%%
translation_unit
    : external_declaration
    | translation_unit external_declaration
    ;

external_declaration
    : declaration
    | expression_statement
    | output_statement
    | selection_statement
    | iteration_statement
    | %empty
    ;

declaration
    : type_specifier init_declaration_list ';'
    ;

init_declaration_list
    : init_declaration_list_element
    {
        operandStack.clear();
    }
    | init_declaration_list ',' init_declaration_list_element
    ;

init_declaration_list_element
    : declarator
    | declarator '=' assignment_expression
    {
        operandStack.pop( OperationType::Binary, "=" );
    }
    | array_declarator
    | pointer_declarator
    | pointer_declarator '=' assignment_expression
    {
        operandStack.pop( OperationType::Binary, "=" );
    }
    ;

pointer_declarator
    : '*' IDENTIFIER
    {
        TypeSpecifier pointedType = symbolTab.getTypeOfIncomingId();

        symbolTab.setTypeOfIncomingId( TypeSpecifier::Address );
        symbolTab.setSuffixOfIncomingId( SymbolType::Address );
        operandStack.pushNewId( $2 );

        SymbolGetter::getAddressSuffixIfPossible( symbolTab.get( $2 ) )->m_pointedType = pointedType;
    }
    ;

array_declarator
    : IDENTIFIER array_dimensions_list
    {
        symbolTab.setSuffixOfIncomingId( SymbolType::Array );
        auto createdId = symbolTab.createId( $1 );

        arrayPacker.setArraySizesOf( createdId );
        symbolTab.pushCreatedId();

        operandStack.push( createdId );
    }
    ;

array_dimensions_list
    : array_dimension
    | array_dimensions_list array_dimension
    ;

array_dimension
    : '[' INT_LITERAL ']'
    {
        arrayPacker.addDimension( $2 );
    }
    ;

declarator
    : IDENTIFIER
    {
        symbolTab.setSuffixOfIncomingId( SymbolType::Variable );
        operandStack.pushNewId( $1 );
    }

primary_expression
    : IDENTIFIER
    {
        operandStack.pushExistingId( $1 );
    }
    | literal
    | '(' expression ')'
    ;

input_expression
    : INPUTI '(' ')'
    {
        operandStack.pushInputInteger();
    }
    | INPUTR '(' ')'
    {
        operandStack.pushInputReal();
    }
    ;

literal
    : INT_LITERAL           { operandStack.push( $1 ); }
    | REAL_LITERAL          { operandStack.push( $1 ); }
    | STRING_LITERAL        { operandStack.push( $1 ); }
    | TRUE                  { operandStack.push( 1 ); }
    | FALSE                 { operandStack.push( 0 ); }
    ;

postfix_expression
    : primary_expression
    | postfix_expression '[' expression ']'
    {
        operandStack.pop( OperationType::Binary, "[]" );
    }
    | postfix_expression INC_OP
    {
        operandStack.pop( OperationType::Unary, "post_inc" );
    }
    | postfix_expression DEC_OP
    {
        operandStack.pop( OperationType::Unary, "post_dec" );
    }
    ;

unary_expression
    : postfix_expression
    | unary_operator unary_expression   { operandStack.pop( OperationType::Unary ); }
    | INC_OP unary_expression
    {
        operandStack.pop( OperationType::Unary, "pre_inc" );
    }
    | DEC_OP unary_expression
    {
        operandStack.pop( OperationType::Unary, "pre_dec" );
    }
    ;

unary_operator
    : '-'                   { operandStack.pushPrefixOperator( "-" ); }
    | '!'                   { operandStack.pushPrefixOperator( "!" ); }
    ;

multiplicative_expression
    : unary_expression
    | input_expression
    | multiplicative_expression '*' unary_expression
    {
        operandStack.pop( OperationType::Binary, "*" );
    }
    | multiplicative_expression '/' unary_expression
    {        
        operandStack.pop( OperationType::Binary, "/" );
    }
    | multiplicative_expression '%' unary_expression
    {
        operandStack.pop( OperationType::Binary, "%" );
    }
    ;

additive_expression
    : multiplicative_expression
    | additive_expression '+' multiplicative_expression
    {
        operandStack.pop( OperationType::Binary, "+" );
    }
    | additive_expression '-' multiplicative_expression
    {
        operandStack.pop( OperationType::Binary, "-" );
    }
    ;

relational_expression
    : additive_expression
    | relational_expression '<' additive_expression
    {
        operandStack.pop( OperationType::Binary, "<" );
    }
    | relational_expression '>' additive_expression
    {
        operandStack.pop( OperationType::Binary, ">" );
    }
    | relational_expression LE_OP additive_expression
    {
        operandStack.pop( OperationType::Binary, "<=" );
    }
    | relational_expression GE_OP additive_expression
    {
        operandStack.pop( OperationType::Binary, ">=" );
    }
    ;

equality_expression
    : relational_expression
    | equality_expression EQ_OP relational_expression
    {
        operandStack.pop( OperationType::Binary, "==" );
    }
    | equality_expression NE_OP relational_expression
    {
        operandStack.pop( OperationType::Binary, "!=" );
    }
    ;

logical_and_expression
    : equality_expression
    | logical_and_expression AND_OP equality_expression
    {
        operandStack.pop( OperationType::Binary, "&&" );
    }
    ;

logical_or_expression
    : logical_and_expression
    | logical_or_expression OR_OP logical_and_expression
    {
        operandStack.pop( OperationType::Binary, "||" );
    }
    ;

assignment_expression
    : logical_or_expression
    | allocation_expression
    | unary_expression '=' assignment_expression
    {
        operandStack.pop( OperationType::Binary, "=" );
    }
    ;

allocation_expression
    : NEW type_specifier '[' assignment_expression ']'
    {
        operandStack.popAllocatedAddress();
    }
    ;

expression
    : assignment_expression
    ;

type_specifier
    : INT
    {
        symbolTab.setTypeOfIncomingId( TypeSpecifier::Integer );
    }
    | REAL
    {
        symbolTab.setTypeOfIncomingId( TypeSpecifier::Real );
    }
    ;

statement
    : compound_statement
    | expression_statement
    | selection_statement
    | iteration_statement
    | jump_statement
    | output_statement
    ;

compound_statement
    : '{' '}'
    | '{' block_item_list '}'
    ;

block_item_list
    : block_item
    | block_item_list block_item
    ;

block_item
    : declaration               { }
    | statement                 { }
    ;

output_statement
    : PRINT '(' print_inclusions_string ',' print_arguments_list ')' ';'
    {
        printWrapper.printChain();
        operandStack.clear();
    }
    | PRINTLN '(' print_inclusions_string ',' print_arguments_list ')' ';'
    {
        printWrapper.setIsExtendedPrintln();
        printWrapper.printChain();
        operandStack.clear();
    }
    | PRINT '(' expression ')' ';'
    {
        operandStack.pop( OperationType::Unary, "print" );
        operandStack.clear();
    }
    | PRINTI '(' expression ')' ';'
    {
        operandStack.pop( OperationType::Unary, "printi" );
        operandStack.clear();
    }
    | PRINTR '(' expression ')' ';'
    {
        operandStack.pop( OperationType::Unary, "printr" );
        operandStack.clear();
    }
    | PRINTLN '(' expression ')' ';'
    {
        operandStack.pop( OperationType::Unary, "println" );
        operandStack.clear();
    }
    | PRINTILN '(' expression ')' ';'
    {
        operandStack.pop( OperationType::Unary, "printiln" );
        operandStack.clear();
    }
    | PRINTRLN '(' expression ')' ';'
    {
        operandStack.pop( OperationType::Unary, "printrln" );
        operandStack.clear();
    }
    | PRINTLN '('')' ';'
    {
        codeGenerator.writeNewLine();
    }
    ;

print_inclusions_string
    : STRING_LITERAL
    {
        printWrapper.splitInclusionsString( $1 );
    }
    ;

print_arguments_list
    : print_token  
    | print_arguments_list ',' print_token
    ;

print_token
    : logical_or_expression
    {
        operandStack.pop( OperationType::Unary, "print_token" );
    }
    ;

expression_statement
    : ';'
    | expression ';'
    {
        operandStack.clear();
    }
    ;

selection_statement
    : if_begin statement else_begin statement
    {
        codeGenerator.placeLabel( labelStack.getTop() );
        labelStack.pop();
    }
    | if_begin statement
    {
        codeGenerator.placeLabel( labelStack.getTop() );
        labelStack.pop();
    }
    ;

if_begin
    : IF '(' expression ')'
    {
        labelStack.push();
        operandStack.pop( OperationType::Unary, "if" );
        operandStack.clear();
    }
    ;

else_begin
    : ELSE
    {
        labelStack.push();
        labelStack.saveTop();
        codeGenerator.placeJumpTo( labelStack.getTop() );
        labelStack.pop();
        codeGenerator.placeLabel( labelStack.getTop() );
        labelStack.pop();
        labelStack.pushSaved();
    }
    ;

iteration_statement
    : while_begin while_conditional_expression statement
    {
        labelStack.saveTop();
        labelStack.pop();
        codeGenerator.placeJumpTo( labelStack.getTop() );
        labelStack.pop();
        labelStack.pushSaved();
        codeGenerator.placeLabel( labelStack.getSaved() );
        labelStack.pop();
    }
    | FOR '(' for_initial_statement for_conditional_expression for_pre_statement statement
    {
        codeGenerator.placeJumpTo( labelStack.getTop() );
        labelStack.pop(2);
        codeGenerator.placeLabel( labelStack.getTop() );
        labelStack.pop(2);
    }
    | FOR '(' for_initial_statement for_conditional_expression for_action_expression for_pre_statement statement
    {
        codeGenerator.placeJumpTo( labelStack.getTop() );
        labelStack.pop(2);
        codeGenerator.placeLabel( labelStack.getTop() );
        labelStack.pop(2);
    }
    ;

while_begin
    : WHILE
    {
        labelStack.push();
        codeGenerator.placeLabel( labelStack.getTop() );
    }
    ;

while_conditional_expression
    : '(' expression ')'
    {
        labelStack.push();
        operandStack.pop( OperationType::Unary, "while" );
        operandStack.clear();
    }
    ;

for_initial_statement
    : expression_statement
    {
        labelStack.push();
        labelStack.saveTop();
        codeGenerator.placeLabel( labelStack.getTop() );
    }
    | declaration
    {
        labelStack.push();
        labelStack.saveTop();
        codeGenerator.placeLabel( labelStack.getTop() );
    }
    ;

for_conditional_expression
    :  expression ';'
    {
        labelStack.push();
        operandStack.pop( OperationType::Unary, "for" );

        labelStack.push();
        codeGenerator.placeJumpTo( labelStack.getTop() );

        labelStack.push();
        codeGenerator.placeLabel( labelStack.getTop() );
    }
    | ';'
    ;

for_action_expression
    :  expression
    {
        labelStack.pushSaved();
        codeGenerator.placeJumpTo( labelStack.getTop() );
        labelStack.pop();
        labelStack.saveTop();
        labelStack.pop();

        labelStack.evaluateActionExpression( true );
    }
    ;

for_pre_statement
    : ')'
    {
        operandStack.clear();
        if( labelStack.isActionExpressionUsed() )
        {
            codeGenerator.placeLabel( labelStack.getTop() );
            labelStack.pushSaved();
        }
        else
        {
            labelStack.pop();
            codeGenerator.placeLabel( labelStack.getTop() );
            labelStack.pushSaved();

        }
        labelStack.evaluateActionExpression( false );
    }
    ;

jump_statement
    : CONTINUE ';'
    | BREAK ';'
    | RETURN ';'
    | RETURN expression ';'
    ;
%%

int main( int argc, char** argv )
{
#   if YYDEBUG == 1
        extern int yydebug;
        yydebug = 1;
#   endif

    char c;

    while ( ( c = getopt ( argc, argv, "tv" ) ) != -1 )
    {
        switch (c)
        {
            case 't':
                TriplesLogger::setEnabled();
                break;

            case 'v':
                DebugLogger::setEnabled();
                break;
            
            case '?':
                if ( isprint ( optopt ) )
                    fprintf ( stderr, "Unknown option `-%c'.\n", optopt );
                else
                    fprintf ( stderr,
                            "Unknown option character `\\x%x'.\n",
                            optopt );
                return 1;

            default:
                exit(1);
        }
    }
    
    if( optind == argc )
    {
        yyerror( "No input file detected." );
    }
    else
    {
        string filename = argv[optind];
        string suffix = "cmm";

        if( filename.size() >= suffix.size() &&
           filename.compare( filename.size() - suffix.size(), suffix.size(), suffix) == 0 )
        {
            yyin = fopen( filename.c_str(), "r" );
        }
        else
        {
            yyerror( "Source file requires .cmm suffix." );
        }
    }

    

	yyparse();

    return 0;
}
