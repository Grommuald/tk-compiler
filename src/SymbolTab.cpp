#include "SymbolTab.hpp"
#include "CodeGenerator.hpp"

#include <algorithm>
#include <vector>
#include <fstream>
#include <iostream>
#include <utility>
#include <cassert>

using namespace std;

extern CodeGenerator& codeGenerator;
extern "C" int yyerror( const char* msg, ... );

SymbolTab& SymbolTab::Instance()
{
    static SymbolTab myInstance;
    return myInstance;
}

SymbolTab::SymbolTab()
{

}

SymbolTab::~SymbolTab()
{
    for( auto& it : m_symbolMap )
        if ( it.second != nullptr )
            delete it.second;
}

void SymbolTab::setTypeOfIncomingId( const TypeSpecifier& typeSpecifier )
{
    m_lastPushedType = typeSpecifier;
}

void SymbolTab::setSuffixOfIncomingId( const SymbolType& suffix )
{
    m_lastPushedSuffix = suffix;
}

void SymbolTab::setIdOfIncomingSymbol( const string& id )
{
    m_incomingId = id;
}

void SymbolTab::setAddressTypeOfIncomingId( const TypeSpecifier& typeSpecifier )
{
    m_typeOfIncomingAddressVariable = typeSpecifier;
}

void SymbolTab::pushId( const string& id )
{
    if( exists( id ) )
    {
        std::string message = "Redefinition of identifier: " + id;
        yyerror( message.c_str() );
    }

    m_symbolMap[id] = new Symbol( m_lastPushedType, m_lastPushedSuffix );
    
    auto pair = make_pair( id, m_symbolMap[id] );
    if( SymbolGetter::isAddress( pair ) )
    {
        SymbolGetter::getAddressSuffixIfPossible( pair )->m_pointedType = m_typeOfIncomingAddressVariable;
    }
    codeGenerator.pushData( pair );
    m_lastPushedSuffix = SymbolType::Literal;
}

idSymbolPair SymbolTab::createId( const string& id )
{
    if( exists( id ) )
    {
        std::string message = "Redefinition of identifier: " + id;
        yyerror( message.c_str() );
    }
  
    m_symbolMap[id] = new Symbol( m_lastPushedType, m_lastPushedSuffix );
    m_createdId = id;

    return make_pair( id, m_symbolMap[id] );
}

void SymbolTab::pushCreatedId()
{
    assert( exists( m_createdId ) == true );
    codeGenerator.pushData( make_pair( m_createdId, m_symbolMap[m_createdId] ) );

    m_lastPushedSuffix = SymbolType::Literal;
}

Symbol* SymbolTab::get( const string& id )
{
    return m_symbolMap.find( id )->second;
}

TypeSpecifier SymbolTab::getTypeOfIncomingId() const
{
    return m_lastPushedType;
}

string SymbolTab::getIdOfIncomingSymbol() const
{
    return m_incomingId;
}

TypeSpecifier SymbolTab::getAddressTypeOfIncomingId( ) const
{
    return m_typeOfIncomingAddressVariable;
}

bool SymbolTab::exists( const string& id ) const
{
    return m_symbolMap.find( id ) != m_symbolMap.end();
}

SymbolTab& symbolTab = SymbolTab::Instance();
