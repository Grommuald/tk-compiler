#include <stack>
#include <string>
#include <vector>
#include <utility>
#include <map>

#include "Symbolic.hpp"

#ifndef OPERAND_STACK_HPP
#define OPERAND_STACK_HPP

class OperandStack
{
    OperandStack( OperandStack const& ) = delete;
    OperandStack( OperandStack&& ) = delete;
    OperandStack& operator=( OperandStack const& ) = delete;
    OperandStack& operator=( OperandStack && ) = delete;

public:
    static OperandStack& Instance();

    void push( const int& val );
    void push( const std::string& val );
    void push( const float& val );
    void push( const idSymbolPair& pair );
    void push( idSymbolPair&& pair );
    void pushNewId( const std::string& id );
    void pushNewId();
    void pushExistingId( const std::string& id );
    void pushPrefixOperator( const std::string& _operator );
    void pushInputInteger();
    void pushInputReal();
    
    void pop( const OperationType& operationType, const std::string& _operator );
    void pop( const OperationType& operationType, const char& _operator );
    void pop( const OperationType& operationType );
    void popAllocatedAddress();
    void clear();

private:
    OperandStack();
    ~OperandStack();

    uint8_t getOperandsNumber( const OperationType& type ) const;
    std::string getAvailableTemporaryVariableId() const;

    std::vector<idSymbolPair> popNext( 
        const uint32_t& numberOfOperands,
        const OperationType& operationType
    );
    bool isTemporaryVariableAvailable(
        const TypeSpecifier&
    );
    bool isTemporaryAddressAvailable(
        const TypeSpecifier&
    );
    void tryToUnlockTemporaryVariable(
        std::vector<idSymbolPair>&
    );
    void setArraySuffixForIncomingAddress( 
        const idSymbolPair&
    );
    void setArrayIdForIncomingAddress( 
        const idSymbolPair& arrayOperand 
    );
    void setNewIncomingTypeBasedOnOperandsType(
        const std::vector<idSymbolPair>&
    );
    void checkIfNewTemporaryVariableIsNeeded(
        std::vector<idSymbolPair>&,
        const std::string&
    );
    bool checkIfNewAddressVariableIsNeeded( 
        idSymbolPair&,
        const std::string&
    );
    void loadInputSymbols(
        std::vector<idSymbolPair>&
    );
    void dereferenceAddressesIfNeeded(
        std::vector<idSymbolPair>& operands,
        const std::string& 
    );
    void doTypeCastingIfNeeded(
        std::vector<idSymbolPair>&,
        const std::string&,
        const OperationType&
    );
    void checkAndPushId(
        const TypeSpecifier&,
        const SymbolType& = SymbolType::Variable
    );
    void pushNewTemporaryVariable();
    void printTemporaryIdsStack();
   
    std::string m_designatedAvailableTempId;

    //[[deprecated]] 
    std::vector<std::string> m_triples;
    
    std::stack<idSymbolPair> m_stack;
    std::stack<std::string> m_incomingPrefixOperators;
    std::vector<std::string> m_temporaryIdsStack;
    std::string m_currentTemporaryValueName;

    uint32_t m_floatLiteralsNameNumber = 0;
    uint32_t m_stringLiteralsNameNumber = 0;
    uint32_t m_temporaryVariableNameNumber = 0;
    uint32_t m_temporaryAddressNameNumber = 0;

    bool m_doesNewNameNeedToBeSet = false;
    bool m_isTempAddrNeededToBeRemoved = false;
    
    std::map<std::string, std::string> m_stringLiteralsMap;
    std::map<float, std::string> m_floatsLiteralsMap;

    std::map<std::string, std::pair<TypeSpecifier, bool>> m_temporaryVariablesMap;
    std::map<std::string, std::pair<TypeSpecifier, bool>> m_temporaryAddressesMap;

    ArraySuffix* m_incomingArraySuffix;
    std::string m_incomingArrayId;
};

#endif
