#include "Symbolic.hpp"

#ifndef ARRAY_PACKER_HPP
#define ARRAY_PACKER_HPP

class ArrayPacker
{
    ArrayPacker( ArrayPacker const& ) = delete;
    ArrayPacker( ArrayPacker&& ) = delete;
    ArrayPacker& operator=( ArrayPacker const& ) = delete;
    ArrayPacker& operator=( ArrayPacker && ) = delete;

public:
    static ArrayPacker& Instance();

    void addDimension( const uint32_t& );
    void setArraySizesOf( const idSymbolPair& );

private:
    ArrayPacker();
    ~ArrayPacker();

    std::vector<uint32_t> m_dimensionSizes;
};

#endif


