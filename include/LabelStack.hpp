#include <string>
#include <stack>

#ifndef LABEL_STACK_HPP
#define LABEL_STACK_HPP

class LabelStack
{
    LabelStack( LabelStack const& ) = delete;
    LabelStack( LabelStack&& ) = delete;
    LabelStack& operator=( LabelStack const& ) = delete;
    LabelStack& operator=( LabelStack && ) = delete;

public:
    static LabelStack& Instance();

    void pop( uint32_t = 1 );
    void push();
    void pushSaved();
    void saveTop();
    std::string getSaved() const;
    std::string getTop() const;

    void evaluateActionExpression( const bool& val );
    bool isActionExpressionUsed() const;

private:
    LabelStack();
    ~LabelStack();

    uint32_t m_currentLabelNumber = 0;

    std::string m_savedLabel;
    std::stack<std::string> m_labelStack;

    bool m_isActionExpressionUsed = false;
};

#endif
