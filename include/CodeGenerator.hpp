#include <string>
#include <fstream>
#include <vector>
#include <utility>
#include <initializer_list>

#ifndef CODE_GENERATOR_HPP
#define CODE_GENERATOR_HPP

struct Symbol;
struct Value;

class CodeGenerator
{
    using idSymbolPair = std::pair<std::string, Symbol*>;

    CodeGenerator( CodeGenerator const& ) = delete;
    CodeGenerator( CodeGenerator&& ) = delete;
    CodeGenerator& operator=( CodeGenerator const& ) = delete;
    CodeGenerator& operator=( CodeGenerator && ) = delete;

public:
    static CodeGenerator& Instance();

    void pushData( const idSymbolPair& data );
    void pushData( const idSymbolPair& data, const int& );
    void pushData( const idSymbolPair& data, const float& );
    void pushData( const idSymbolPair& data, std::string );

    void putText( const std::string& );
    void putData( const std::string& );
    
    void pushBinaryOperation(
        const std::string& _operator,
        const idSymbolPair& lhs,
        const idSymbolPair& rhs,
        const std::string& resultVarName
    );
    void pushUnaryOperation(
        const std::string& _operator,
        const idSymbolPair& rhs,
        const std::string& resultVarName
    );

    void setValue( 
        const idSymbolPair& lhs, 
        const idSymbolPair& rhs, 
        const std::string& = "" 
    );
    void placeLabel( const std::string& labelName );
    void placeJumpTo( const std::string& labelName );
    void loadInputTo( const idSymbolPair& symbol );
    void loadAddressToVariable(
        const idSymbolPair& indexValueVariable,
        const idSymbolPair& arrayVariable,
        const std::string& resultVarName
    );
    void castToFloat(
        const idSymbolPair& src,
        const idSymbolPair& dst
    );
    void dereferenceToInt(
        const idSymbolPair& src,
        const idSymbolPair& dst
    );
    void dereferenceToFloat(
        const idSymbolPair& src,
        const idSymbolPair& dst
    );
    void castToInt(
        const idSymbolPair& src,
        const idSymbolPair& dst
    );
    void writeNewLine();
    void placeMarker( const std::string& marker );
    void zeroAddress( const idSymbolPair& );
    
private:
    void loadOperandsToRegisters(
        std::initializer_list< idSymbolPair > operands,
        const std::string& registerName = "t",
        const bool& useNumeration = true,
        const bool& forceInteger = false
    );
    void trimRedundantSaveLoads();
    void setRegisterIfFloatComparisonTrue(
        const std::string& condition,
        const std::string& resultVarName
    );
    void setRegisterIfIntComparisonTrue(
        const std::string& condition,
        const std::string& resultVarName
    );

    std::string getCurrentBranchFlagName() const;

    CodeGenerator();
    ~CodeGenerator();

    std::vector< std::string > m_dataSection;
    std::vector< std::string > m_textSection;

    std::ofstream m_outputFile;

    uint32_t m_branchFlagNumber = 0;
};

#endif
