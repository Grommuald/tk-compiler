#include <string>

#ifndef DEBUG_HPP
#define DEBUG_HPP

class DebugLogger
{
public:
    static void log( const std::string&, const std::string&... );
    static void setEnabled();

private:
    static bool enabled;
};

#endif