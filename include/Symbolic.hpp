#include <string>
#include <vector>
#include <iostream>
#include <utility>

#ifndef SYMBOLIC_HPP
#define SYMBOLIC_HPP

enum class OperationType
{
    Unary,
    Binary
};

enum class TypeSpecifier
{
    Integer,
    Real,
    String,
    Address
};

enum class SymbolType
{
    Literal,
    Variable,
    Function,
    Address,
    Array,
    Input
};

struct TypeSuffix
{
    TypeSuffix();
    virtual ~TypeSuffix();
};

struct FunctionSuffix : public TypeSuffix
{
    void pushParam( const std::string& id );
    std::vector<std::string> m_paramsIds;
};

struct ArraySuffix : public TypeSuffix
{
    std::vector< uint32_t > m_dimensions;
    uint32_t m_numberOfBytesToAllocate;
};

struct AddressSuffix : public TypeSuffix
{
    ArraySuffix* m_rootArray = nullptr;
    TypeSpecifier m_pointedType;

    std::string m_pointedArrayId;
    uint8_t m_currentNestLevel = 0;
    bool m_isReadyToDereference = false;
};

struct Symbol
{
    Symbol( const TypeSpecifier&, const SymbolType& = SymbolType::Literal );
    ~Symbol();

    TypeSpecifier m_typeSpecifier;
    SymbolType m_symbolType;
    TypeSuffix* m_suffix;
};

using idSymbolPair = std::pair<std::string, Symbol*>;

class SymbolGetter
{
public:
    static std::string getId( const idSymbolPair& );
    static Symbol* getSymbol( const idSymbolPair& );
    static TypeSpecifier getType( const idSymbolPair& );
    static SymbolType getSpecial( const idSymbolPair& );
    
    static ArraySuffix* getArraySuffixIfPossible( const idSymbolPair& );
    static FunctionSuffix* getFunctionSuffixIfPossible( const idSymbolPair& );
    static AddressSuffix* getAddressSuffixIfPossible( const idSymbolPair& );
    static AddressSuffix* getAddressSuffixIfPossible( Symbol* );
    
    static bool isAddress( const idSymbolPair& );
    static bool isReal( const idSymbolPair& );
    static bool isInteger( const idSymbolPair& );
    static bool isString( const idSymbolPair& );

    static bool isScalar( const idSymbolPair& );
    static bool isArray( const idSymbolPair& );
};

#endif
