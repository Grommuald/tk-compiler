#include <vector>
#include <string>
#include <utility>
#include "Symbolic.hpp"

#ifndef PRINT_WRAPPER_HPP
#define PRINT_WRAPPER_HPP

class PrintWrapper
{
    PrintWrapper( PrintWrapper const& ) = delete;
    PrintWrapper( PrintWrapper&& ) = delete;
    PrintWrapper& operator=( PrintWrapper const& ) = delete;
    PrintWrapper& operator=( PrintWrapper && ) = delete;

public:
    static PrintWrapper& Instance();

    void splitInclusionsString( const std::string& );
    void printChain();
    void passToken( idSymbolPair );
    void setIsExtendedPrintln();

private:
    PrintWrapper();
    ~PrintWrapper();

    std::vector<std::string> m_inclusionsStringParts;
    std::vector<idSymbolPair> m_passedSymbols;

    bool m_isPrintln = false;
};

#endif
