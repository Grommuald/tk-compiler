#include "Symbolic.hpp"

#include <string>
#include <map>
#include <stack>

#ifndef SYMBOL_TAB_HPP
#define SYMBOL_TAB_HPP

class SymbolTab
{
    SymbolTab( SymbolTab const& ) = delete;
    SymbolTab( SymbolTab&& ) = delete;
    SymbolTab& operator=( SymbolTab const& ) = delete;
    SymbolTab& operator=( SymbolTab && ) = delete;

public:
    static SymbolTab& Instance();

    void setTypeOfIncomingId( const TypeSpecifier& );
    void setSuffixOfIncomingId( const SymbolType& );
    void setIdOfIncomingSymbol( const std::string& );
    void setAddressTypeOfIncomingId( const TypeSpecifier& );

    void pushId( const std::string& );

    idSymbolPair createId( const std::string& id );
    void pushCreatedId();

    Symbol* get( const std::string& id );
    TypeSpecifier getTypeOfIncomingId() const;
    TypeSpecifier getAddressTypeOfIncomingId() const;
    std::string getIdOfIncomingSymbol() const;
    bool exists( const std::string& id ) const;

private:
    std::map<std::string, Symbol*> m_symbolMap;

    TypeSpecifier m_lastPushedType;
    TypeSpecifier m_typeOfIncomingAddressVariable;
    
    SymbolType m_lastPushedSuffix;
    std::string m_incomingId;
    std::string m_createdId;

    SymbolTab();
    ~SymbolTab();
};

#endif
