#include <string>
#include <fstream>

#ifndef TRIPLES_LOGGER
#define TRIPLES_LOGGER

class TriplesLogger
{

public:
    static void pushToken( const std::string& );
    static void pushNewLine();
    static void setEnabled();

private:
    static std::ofstream outputFile;
    static std::string outputFileName;
    static bool enabled;
};

#endif